## Prerequisite

The reader should be familiar with the parametrization of manifolds in general and with the Mercator projection specifically.

## Motivation

english bellow

Bei der Zylinderprojektion einer Kugeloberfläche werden die Breitenkreise um den reziproken Wert des Cosinus des Breitengrades längen-gedehnt; möchte man eine winkeltreue Abbildung konstruieren, scheint es naheliegend, die Längenkreise um eben diesen Faktor zu strecken, was aber nicht das gewünschte Resultat liefert. Stattdessen müssen die Längenkreise um den Integralwert dieses Faktors gestreckt werden, d.h, die 1/cos Dehnung der Meridiane ist lokal zu betrachten, die globale Dehnung wird durch das Integral bewerkstelligt. Da dies (zumindest für mich) nicht unmittelbar einsichtig war, und da ich auch mit der in der (englischen) Wikipedia-Seite gegebenen Erklärung nicht ganz glücklich wurde, findet sich hier eine differentialgeometrische Herleitung der Gudermann Funktion, welche der Inversen des entsprechenden Integralfaktors entspricht. (Die Gudermann Funktion beinhaltet die Schlüssel-Eigenschaft der Mercator Projektion; sie beschreibt im wesentlichen die Abweichung von der Parametrisierung durch Kugelkoordinaten, i.e. den Unterschied zu einer quadratischen Plattkarte; diese entsteht durch Approximation der Gudermann Funktion durch die Identitätsabbildung. (Zulässig in Aequator-Nähe, unzulässig in der Nähe der Pole).)


Even with modern math it is all but trivial to derive the key-property of the Mercator projection. (Being both a conformal and cylindrical map).

## Literatur

John Vermeulen: Zwischen Gott und der See

