import numpy as np

import matplotlib.pyplot as plt
def gudermannian(x):
    return 2*np.arctan(np.exp(x)) - 0.5*np.pi


x = np.arange(-5.0, 5.0, 0.1)
plt.plot(x, 360/(2*np.pi)*gudermannian(x), label='gudermannian')
plt.plot(x, 360/(2*np.pi)*np.arctan(x), label='arctan')
plt.xlabel('Meridian Coordinate'); plt.ylabel('Latitude')
plt.legend(); plt.show()
