\NeedsTeXFormat{LaTeX2e}
\documentclass[11pt,a4paper]{article}
\usepackage{a4,latexsym}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{amssymb}

\author{A. Studer}
\title{Mercator Projection}


\begin{document}
\maketitle

\begin{abstract}
Derivation of the Gudermannian function used for the Mercator projection.
(How to stretch meridians to get cylindrical conformality)
\end{abstract}

\section{Metric Tensor}
Let $(x,y)$ be the parameter coordinates defining the Mercator projection.
The manifold 'surface-of-the-earth' is parameterized as 
$M: \mathbb{R}^2 \rightarrow \mathbb{S}^2 \backslash \{\pm 1 \}$

\begin{equation}
(x,y) \rightarrow \Big(\sin(\pi/2 - f(y))\cos(x), \sin(\pi/2 - f(y))\sin(x), \cos(\pi/2 - f(y))\Big)
\end{equation}
where $f$ is a yet to be defined function yielding the polar angle 
(measured from equator, $f(y) \in [-\pi/2, \pi/2]$) and $x \in [0,2\pi]$ equals the azimuthal angle. 
(The radius of the earth is set to one for simplicity).
The above $\pi/2$ shift with regard to spherical coordinates is owed to the convention that
the equator has $y$ coordinate zero in Mercator projection. We simplify the parametrization

\begin{equation}
M:\quad
(x,y) \rightarrow \Big(\cos(f(y))\cos(x), \cos(f(y))\sin(x), \sin(f(y))\Big)
\end{equation}

To calculate the metric tensor, $g_{ij} = \langle \partial_i M, \partial_j M \rangle$
we note
\begin{equation}
\partial_x M = \Big(-\cos(f(y))\sin(x), \cos(f(y))\cos(x), 0 \Big)
\end{equation}

\begin{equation}
\partial_y M = f'(y) \Big(-\sin(f(y))\cos(x), -\sin(f(y))\sin(x), \cos(f(y)) \Big)
\end{equation}

hence
\begin{equation}
\langle \partial_x M, \partial_x M \rangle
= ||\partial_x M||^2
= \cos^2(f(y))
\end{equation}

\begin{equation}
\langle \partial_y M, \partial_y M \rangle
= ||\partial_y M||^2
= f'(y)^2
\end{equation}

\begin{equation}
\langle \partial_x M, \partial_y M \rangle
=
f'(y)\Big(\cos(f)\sin(x)\sin(f)\cos(x) - \cos(f)\cos(x)\sin(f)\sin(x)  \Big)
\end{equation}

The last term (the brackets) evaluating to zero shows that the metric tensor is diagonal.
\newpage
\section{Conformal Map}
For the metric tensor to be proportional to unity (this is the key property of the Mercator projection)
it must hold
\begin{equation}
\langle \partial_x M, \partial_x M \rangle = \langle \partial_y M, \partial_y M \rangle
\end{equation}
or
\begin{equation}
\cos^2(f(y))
=
f'(y)^2
\quad
\Leftrightarrow
\quad
f'(y) = \cos(f(y))
\end{equation}
the equivalence holds since for $f(y)$ between -90 and 90 degrees, the cosine is positive.
The above (non-linear) differential equation is solved by the Gudermannian function $gd$,
since this function has the properties

\begin{equation}
gd' = sech
\end{equation}

and

\begin{equation}
\cos \circ gd = sech
\end{equation}
see e.g. the Wolfram MathWorld page entry for 'Gudermannian'.\newline
This completes the proof.\newline
Mercator's achievement is astounding given the fact that he did not know calculus,
let alone differential geometry (16th century)

\section{Shortcomings}
The angle-preserving property of the mapping (which greatly simplifies navigation)
comes at a price: Geodesics (on the map) are not mapped to geodesics (on the globe)
but to loxodromes. A loxodrome $L(\lambda)$ is defined as a path $M((x_0,y_0) + \lambda \vec e)$
where $\vec e$ is some (unit-)vector $\in \mathbb{R}^2$ describing the direction of travel,\footnote{
Primarily on the map but due to the conformal property also on the globe, e.g. as defined by a compass.
Proof: Let $dm_1, dm_2$ be two vectors in the map attached to a coordinate $m$. These vectors are 
mapped to the globe (to the tangent space at $M(m)$ to be precise) $DM dm_1, DM dm_2$, $DM$ labeling
the Jacobi Matrix. The angle between the vectors in tangent space is determined by
$\frac{ \langle DMdm_1, DMdm_2 \rangle } { \sqrt \langle DMdm_1, DMdm_1 \rangle \sqrt \langle DMdm_2, DMdm_2\rangle }=
\frac{ \langle dm_1, (DM)^T DMdm_2 \rangle } { \sqrt \langle dm_1, (DM)^T DMdm_1 \rangle \sqrt \langle dm_2, (DM)^TDMdm_2\rangle} $. Since $(DM)^T DM$ equals the metric tensor which is made diagonal, $g(m) = \eta(m) I$, the angle evaluates as
$  \frac{ \eta \langle dm_1, dm_2 \rangle } { \sqrt \eta \sqrt \langle dm_1, dm_1 \rangle \sqrt \eta \sqrt \langle dm_2, dm_2\rangle }$. This cancels to $  \frac{ \langle dm_1, dm_2 \rangle } { \sqrt \langle dm_1, dm_1 \rangle \sqrt \langle dm_2, dm_2\rangle }$ which equals the angle (cosine) in the map.} 
starting from $M(x_0,y_0)$. Unless $\vec e = \vec e_y$ (sailing towards a pole) or $\vec e = \vec e_x$ 
and $(x_0,y_0) = (x_0, 0)$ (sailing along equator), $L(\lambda)$ is not the shortest path between 
$M(x_0,y_0)$ and $M((x_0,y_0) + \lambda_{max} \vec e)$. The shortest path between any two points is 
called an orthodrome (section of a great circle). \newline
The distortion of area is of minor concern in nautics.

\section{Mercator- versus Stereographic-Projection}
The stereographic projection is also conformal, but a straight line drawn on the map will form different angles
with the curved meridians, such that in this case conformality becomes useless for navigation.
So this is the other key property of the Mercator projection: All meridians form straight parallel lines, such that
the angular deviation from north remains constant for a straight course-line drawn on the map.
(The stereographic map is conformal but not cylindrical).\newline \newline

\begin{figure}[ht!]
\centering
\includegraphics[width=115mm]{MercStereo.jpg}
\caption{Mercator (and Son's) stereographic projection of the world (1587) ... \label{overflow}}
\end{figure}


\begin{figure}[ht!]
\centering
\includegraphics[width=115mm]{MercMerc.png}
\caption{... and his masterpiece, the 1569 Mercator world map \label{overflow}}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=110mm]{Equirectangular.jpg}
\caption{Example of a cylindrical but not conformal map. ($f = id$, so basically \newline parametrization according spherical coordinates,
called a \textit{equirectangular} map) \label{overflow}}
\end{figure}


\section{Missing Physical Interpretation}
The Mercator projection has no direct physical interpretation and hence cannot be visualized using light rays.
It is assumed that Mercator was experimenting with a lightsource (candle) within a semitransparent globe,
projecting the shadows of the meridians on a cylinder wrapped around the globe, but nevertheless he was seemingly
aware of the fact that this optical projection was not representing the correct solution; this is amazing, since
the true solution (the gudermannian) is numerically\footnote{Concerning \textit{reasoning} the two approaches are far away from each other}
quite close to the optical solution: 
A simple geometric calculation yields that the optical projection maps the meridians as $\varphi = arctan(y)$ as 
compared to $\varphi = f(y) = gd(y)$, where $\varphi$ is the latitude and $y$ its coordinate in the map.
The bellow plot shows a comparison between the true solution and the intuitive geometrical/optical solution.


\begin{figure}[ht!]
\centering
\includegraphics[height=60mm,width=100mm]{GudvsAtan.png}
\caption{True versus 'naive' solution \label{overflow}}
\end{figure}

\end{document}
