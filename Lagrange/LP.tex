\NeedsTeXFormat{LaTeX2e}
\documentclass[11pt,a4paper]{article}
\usepackage{a4,latexsym}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{listings}
\usepackage{hyperref}

\author{A. Studer}
\title{Lagrange Points}


\begin{document}
\maketitle

\section{Introduction}
There are many sources describing how to find the Lagrange points
$L_1, L_2, L_3$, most of them assuming that both sun and earth orbiting
in \emph{circles} around the common center of mass; here we focus on the 
existence of Lagrange points for \emph{generic} orbits. Derivation is
similar, nevertheless both exercises are carried out.

\section{Lagrange Points $L_1, L_2, L_3$; Focus on $L_2$}
\subsection{Circular motion}
Let denote the satellite's orbit $\bm{r}_s = \lambda_i \bm{r}_{\oplus}$,
for a circular motion, the gravitational forces acting on the satellite
must provide the centripetal force
\begin{equation}
- m_s \omega_s^2 \lambda_i \bm{r}_{\oplus} =
- G \frac{m_s M_{\oplus}}{|\lambda_i \bm{r}_{\oplus} - \bm{r}_{\oplus}|^3}
(\lambda_i \bm{r}_{\oplus} - \bm{r}_{\oplus})
- G \frac{m_s M_{\odot}}{|\lambda_i \bm{r}_{\oplus} - \bm{r}_{\odot}|^3}
(\lambda_i \bm{r}_{\oplus} - \bm{r}_{\odot})
\end{equation}
$\omega_{\oplus}$ derives from the analogous relationship
between gravitational and centripetal force (eq. (7) is used in last step)

\begin{equation}
- M_{\oplus} \omega_{\oplus}^2 \bm{r}_{\oplus} =
- G \frac{M_{\oplus} M_{\odot}}{|\bm{r}_{\oplus} - \bm{r}_{\odot}|^3}
(\bm{r}_{\oplus} - \bm{r}_{\odot})
=
- G \frac{ M_{\oplus} M_{\odot}}{|(1 + \alpha) \bm{r}_{\oplus}|^3}
(1 + \alpha) \bm{r}_{\oplus}
\end{equation}
such that 

\begin{equation}
\omega_{\oplus}^2 =
\frac{ G M_{\odot}}{|(1 + \alpha) \bm{r}_{\oplus}|^3}
(1 + \alpha) 
\end{equation}
Being located at a Lagrange point means that the satellite moves according 
$\omega_s = \omega_{\oplus}$, hence we make this replacement in (1).
Additionally, replacing again the motion of the sun (in (1) rhs) by the scaled motion 
of the earth according center of mass definition (which defines the origin 
of the frame of reference)

\begin{equation}
- m_s \frac{ G M_{\odot}}{|(1 + \alpha) \bm{r}_{\oplus}|^3}
(1 + \alpha) \lambda_i \bm{r}_{\oplus} = 
- \frac{G m_s M_{\oplus}}{|(\lambda_i  - 1) \bm{r}_{\oplus}|^3}
(\lambda_i  - 1) \bm{r}_{\oplus}
- \frac{G m_s M_{\odot}}{|(\lambda_i - \alpha) \bm{r}_{\oplus}|^3}
(\lambda_i - \alpha) \bm{r}_{\oplus}
\end{equation}
Hence we get rid of $m_s$, $G$, $\bm{r}_{\oplus}/|\bm{r}_{\oplus}|^3$
and the sign; dividing by $M_{\odot}$ yields (comp. with (12))

\begin{equation}
\frac{(1 + \alpha)\lambda_i}{|1 + \alpha|^3} = 
\frac{\alpha}{|\lambda_i  - 1|^3}
(\lambda_i  - 1) 
+
\frac{1}{|\lambda_i - \alpha|^3}
(\lambda_i - \alpha)
\end{equation}



\subsection{General closed orbit}
Assume both the earth and sun orbiting in a closed form around the
common center of mass, such that according Newton's laws


\begin{equation}
M_{\oplus} \frac{d^2}{dt^2}\bm{r}_{\oplus}(t) = 
- G \frac{M_{\oplus} M_{\odot}}{|\bm{r}_{\oplus}(t) - \bm{r}_{\odot}(t)|^3}
\big(\bm{r}_{\oplus}(t) - \bm{r}_{\odot}(t)\big)
\end{equation}
The trajectory of the sun's motion is by definition (of the center of mass)

\begin{equation}
\bm{r}_{\odot}(t) = - \frac{M_{\oplus}}{M_{\odot}}\bm{r}_{\oplus}(t)
=: - \alpha \bm{r}_{\oplus}(t)
\end{equation}

The existence of Lagrange points can be formalized as follows:
$\exists \lambda_i \in \mathbf{R}$ such that for e.g. a satellite
with negligible mass $m_s$ the following equation of motion must hold
for its position $\bm{r}_s(t) = \lambda_i \bm{r}_{\oplus}(t)$:

\begin{equation}
m_s \frac{d^2}{dt^2} \lambda_i \bm{r}_{\oplus} =
- G \frac{m_s M_{\oplus}}{|\lambda_i \bm{r}_{\oplus} - \bm{r}_{\oplus}|^3}
(\lambda_i \bm{r}_{\oplus} - \bm{r}_{\oplus})
- G \frac{m_s M_{\odot}}{|\lambda_i \bm{r}_{\oplus} - \bm{r}_{\odot}|^3}
(\lambda_i \bm{r}_{\oplus} - \bm{r}_{\odot})
\end{equation}
where we have used that the satellite always follows the earth-trajectory
(with respect to the center of mass, so the satellite is
perfectly located in the earth's shadow) and both the gravitational forces 
of the sun and the earth determine the satellite's orbit. (But not vice versa)
\newline
On the left hand side of eq. (8) we insert equation (6) and cancel $m_s$ and 
the gravitational constant $G$ including sign:

\begin{equation}
\frac{M_{\oplus} M_{\odot}}{|\bm{r}_{\oplus} - \bm{r}_{\odot}|^3}
(\bm{r}_{\oplus} - \bm{r}_{\odot}) \frac{\lambda_i}{ M_{\oplus}} = 
 \frac{M_{\oplus}}{|\lambda_i \bm{r}_{\oplus} - \bm{r}_{\oplus}|^3}
(\lambda_i \bm{r}_{\oplus} - \bm{r}_{\oplus})
+ \frac{M_{\odot}}{|\lambda_i \bm{r}_{\oplus} - \bm{r}_{\odot}|^3}
(\lambda_i \bm{r}_{\oplus} - \bm{r}_{\odot})
\end{equation}
$\bm{r}_{\odot}$ is replaced by equation (7) and canceling $M_{\oplus}$ on the left yields

\begin{equation}
\frac{\lambda_i M_{\odot}}{|(1 + \alpha)\bm{r}_{\oplus}|^3}
(1 + \alpha)\bm{r}_{\oplus} = 
\frac{M_{\oplus}}{|(\lambda_i  - 1) \bm{r}_{\oplus}|^3}
(\lambda_i  - 1) \bm{r}_{\oplus}
+ \frac{M_{\odot}}{|(\lambda_i - \alpha) \bm{r}_{\oplus}|^3}
(\lambda_i - \alpha) \bm{r}_{\oplus}
\end{equation}

Now the good news is we get rid of the earth's orbit (whatever it may be)
and arrive at a \emph{time-independent} scalar equation
\begin{equation}
\frac{\lambda_i M_{\odot}}{|1 + \alpha|^3}
(1+ \alpha) = 
\frac{M_{\oplus}}{|\lambda_i  - 1|^3}
(\lambda_i  - 1) 
+ \frac{M_{\odot}}{|\lambda_i - \alpha|^3}
(\lambda_i - \alpha)
\end{equation}
Deviding by $M_{\odot}$ and exploiting that $\alpha > 0$ 

\begin{equation}
\frac{\lambda_i }{(1 + \alpha)^2}
 = 
\frac{\alpha}{|\lambda_i  - 1|^3}
(\lambda_i  - 1) 
+ \frac{1}{|\lambda_i - \alpha|^3}
(\lambda_i - \alpha)
\end{equation}
Lagrange point $L_2$ (where the J.W. satellite is positioned):
There is $\lambda_2 > 1$, such that

\begin{equation}
\frac{\lambda_2 }{(1+ \alpha)^2}
 = 
\frac{\alpha}{(\lambda_2  - 1)^2}
+ \frac{1}{(\lambda_2 - \alpha)^2}
\end{equation}
Multiplying with the first denominator:

\begin{equation}
\lambda_2
 = 
\frac{\alpha(1 +\alpha)^2}{(\lambda_2  - 1)^2}
+ \frac{(1 + \alpha)^2}{(\lambda_2 - \alpha)^2}
\end{equation}
with the second

\begin{equation}
\lambda_2(\lambda_2  - 1)^2
 = 
\alpha(1 + \alpha)^2
+ \frac{(1 + \alpha)^2 (\lambda_2  - 1)^2}{(\lambda_2 - \alpha)^2}
\end{equation}
and finally the third

\begin{equation}
\lambda_2(\lambda_2  - 1)^2 (\lambda_2 - \alpha)^2
 = 
\alpha(1 + \alpha)^2 (\lambda_2 - \alpha)^2
+ (1 + \alpha)^2 (\lambda_2  - 1)^2
\end{equation}
or

\begin{equation}
\lambda_2(\lambda_2  - 1)^2 (\lambda_2 - \alpha)^2
-
\alpha(1 + \alpha)^2 (\lambda_2 - \alpha)^2
- (1 + \alpha)^2 (\lambda_2  - 1)^2
=
0
\end{equation}
Hence we are left with finding the zeros of a 5th order polynomial;
the solution will depend on $\alpha$ as the only parameter of the system.
(And not e.g. of the revolution period of the planet).
(17) is still a complicated equation, but at least there was no need to know 
that the orbits of planets are ellipses.

Since only numerical analysis of the above equation is possible,
we run some python code to yield the result,
plugin $1/33300$ for the ratio of earth to sun mass.

%\lstset{xleftmargin=0.2}
\begin{lstlisting}[frame=single, linewidth=16.5cm]
a = 1.0/333000
  
def f_2(x):  #Lagrange Point L2
  return x*(x-1)**2*(x-a)**2 - a*(1+a)**2*(x-a)**2 - (1+a)**2*(x-1)**2

def f_1(x):  #Lagrange Point L1, only a single sign flip applies
  return x*(x-1)**2*(x-a)**2 + a*(1+a)**2*(x-a)**2 - (1+a)**2*(x-1)**2

from scipy.optimize import root_scalar

sol = root_scalar(f_2, bracket=[1, 2], x0=1, method='brentq')
print (100*(sol.root - 1))

sol = root_scalar(f_1, bracket=[0, 1], x0=1, method='brentq')
print (100*(1 - sol.root))
\end{lstlisting}


The converged value of the above code (L2) yields 1.00379 per cent for the
earth to satellite distance (as measured in 'earth distance from center of mass' units).
The value in Literature is 1.004 per cent. 
(See \url{https://en.wikipedia.org/wiki/Lagrange_point#Solar_System_values}), 
but length unit is with respect to semi-major axis and distances are measured from the sun.
\newline
Value for L1 is 0.9969 per cent. (Lit.: 0.997).
\newline
Note that for elliptic orbits, the \textit{absolute} distance from earth to the Lagrange points 
varies with time.

\newpage

\section{Lagrange Points $L_4, L_5$ (Generic Orbit)}
We proof that the sun, earth and satellite forming a equilateral triangle
is a stable (dynamic) constellation. First, we calulate the force acting on the satellite

\begin{equation}
\bm{F}\big(\bm r_s(t)\big) =
 -\frac{G m_s M_{\oplus}}{r(t)^3} ( \bm{r}_{s}(t) - \bm{r}_{\oplus}(t))
 -\frac{G m_s M_{\odot}} {r(t)^3} ( \bm{r}_{s}(t) - \bm{r}_{\odot} (t))
\end{equation}

where $r(t) = |\bm r(t)| = |-\bm{r}_{\odot}(t) + \bm{r}_{\oplus}(t)|$ is the length of 
one side of the equilateral triangle (whose size may change with time)\newline
The above can be simplified (dropping explicit time dependency)

\begin{equation}
\bm{F}\big(\bm r_s\big) =
 -\frac{G m_s }{r^3} 
 \Big(
 M_{\oplus}(\bm{r}_{s} - \bm{r}_{\oplus})
 +
 M_{\odot} (\bm{r}_{s} + \alpha \bm{r}_{\oplus}) 
 \Big)
\end{equation}
Since $\alpha M_{\odot} = M_{\oplus}$ this can be further simplified

\begin{equation}
\bm{F}\big(\bm r_s \big) =
-\frac{G m_s M_{\Sigma}}{r^3} \bm r_s
\end{equation}
where $M_{\Sigma} = M_{\odot} + M_{\oplus} $.
This overall force is responsible for the acceleration of the satellite, whose
position can be written as $\bm r_s = \bm{r}_{\oplus} + R \bm r$ where $R$
is a suitable $SO(2)$ rotation matrix \emph{independent} of time and $\bm r$
is the distance between earth and sun. Hence

\begin{equation}
\ddot{\bm{r}}_{s}(t) = 
\ddot{\bm{r}}_{\oplus}(t) + R \ddot{\bm{r}}(t)
= 
-\frac{G M_{\odot}}{r^3} \bm r(t)
-\frac{G M_{\Sigma}}{r^3} R \bm r(t)
\end{equation}
where in the last step it was used that $\mu \ddot{\bm r} = -\frac{G M_{\odot} M_{\oplus}}{r^3} \bm r$
where $\mu$ is the reduced mass of the system earth-sun $\mu =  \frac{M_{\odot} M_{\oplus}}{M_{\odot}+ M_{\oplus}}$.
We replace $R \bm{r} = \bm r_s - \bm{r}_{\oplus}$ and 
$\bm r = -\bm{r}_{\odot} + \bm{r}_{\oplus} = (1 + \alpha) \bm{r}_{\oplus} $ such that 
\begin{equation}
\ddot{\bm{r}}_{s} = 
-\frac{G M_{\odot}}{r^3}  (1 + \alpha) \bm{r}_{\oplus}
-\frac{G M_{\Sigma}}{r^3} (\bm r_s - \bm{r}_{\oplus})
\end{equation}
Since $M_{\odot}(1 + \alpha) = M_{\Sigma}$ there is
\begin{equation}
\ddot{\bm{r}}_{s} = 
-\frac{G M_{\Sigma}}{r^3} ( \bm{r}_{\oplus}  + \bm r_s - \bm{r}_{\oplus})
=
-\frac{G M_{\Sigma}}{r^3} \bm r_s
\end{equation}
Such that indeed: if the satellite's orbit is defined as 
$\bm r_s(t) = \bm{r}_{\oplus}(t) + R \bm r(t)$
the acceleration of the satellite equals the gravitational force per mass acting on it. 
(That $R \in SO(2)$ rotates by the correct angle is implicitly
used by the fact that the distance between sun-satellite and earth-satellite equals the
earth-sun distance $r$, i.e. by the fact the the three bodies form a equilateral triangle). 
Again, the earth's orbit was never specified (remains generic).
\newpage
Note that the satellite is on a different orbit than the earth,
contrary to what might be concluded from the illustrations one finds on the web.
To see this, consider the triangle barycenter-earth-satellite and apply
the law of cosines

\begin{equation}
r_s^2 = r_{\oplus}^2 + r^2 - 2r_{\oplus}r \cos \pi/3
\end{equation}
\begin{equation}
r_s^2 = r_{\oplus}^2 + (1 + \alpha)^2 r_{\oplus}^2 - r_{\oplus}r 
\end{equation}
\begin{equation}
r_s^2 = r_{\oplus}^2 + (1 + \alpha)^2 r_{\oplus}^2 - (1 + \alpha)r_{\oplus}^2 
\end{equation}
\begin{equation}
r_s^2 = r_{\oplus}^2\Big(1 + (1 + \alpha)^2  - (1 + \alpha) \Big)
\end{equation}
expanding up to first order in $\alpha$

\begin{equation}
r_s^2 = r_{\oplus}^2(1 + \alpha) \quad
r_s = r_{\oplus} \sqrt{1 + \alpha}
\end{equation}
and expanding the root to first order

\begin{equation}
r_s = r_{\oplus} (1 + \alpha /2)
\end{equation}
Hence the satellite is $r_{\oplus} \alpha /2$ further away from the center of mass
than the earth is. The relative deviation of the orbits is 
$(r_s - r_{\oplus}) / r_{\oplus} = 1/666000$, given the respective time-delay or
advancement. The situation becomes easier if the earth orbit is a circle, then
the satellite's orbit is a circle as well\footnote{
$r_s^2 = r_{\oplus}^2 + (R \bm r)^2 + 2r_{\oplus} |R \bm r| \cos \theta$ where
$\theta = 120$ degrees is the angle between $\bm r_{\oplus}$ and $R \bm r$ which 
is by construction independent of time, hence the length of $\bm r_s$ is 
independent of time if $r_{\oplus}$ is and thus  $\bm r_s$ describes a circle. 
(both $|\bm r|$ and $R$ are time independent.)}
(with a radius larger than the earth's, as calculated above. The difference is
tiny for the earth-sun system but not zero).
\newline
As a final note: In principle there is no need for the orbit to be closed,
at least the math does not speak against Lagrange points for parabolas or hyperbolas.

\end{document}
