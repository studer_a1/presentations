'''
This python script calculates the Lagrange points L1 and L2 according
theory paper in this repo. Lagrange points (satellite location) is
measured from center of mass.
If position is measured from sun (as in Wikipedia data), we have to add 
the vector alpha*r_earth to the location measured from CM, i.e
r_sun-sat = r_cm-sat + r_cm-sun = lambda*r_earth + alpha*r_earth
= (lambda + alpha)*r_earth. But the definition therein is confusing, if
bodies move in perfect circles, no reference should be made to elliptic
terms like semi-major axis. (The semi-major axis seems to be the distance
from earth to CM (?). The CM is a Focus of both ellipses (Sun and earth)
'''

a = 1.0/333000
  
def f_2(x):
  return x*(x-1)**2*(x-a)**2 - a*(1+a)**2*(x-a)**2 - (1+a)**2*(x-1)**2

def f_1(x):
  return x*(x-1)**2*(x-a)**2 + a*(1+a)**2*(x-a)**2 - (1+a)**2*(x-1)**2

from scipy.optimize import root_scalar

sol = root_scalar(f_2, bracket=[1, 2], x0=1, method='brentq')
print ("L2 position in % Earth-CM  units:   ", 100*(sol.root - 1) )
print ("L2 position in % Earth-Sun units:   ", 100*(sol.root - 1)/(1 + a) )
print ("L2 position similar as in Wikipedia:", 100*((sol.root + a) - 1) )

sol = root_scalar(f_1, bracket=[0, 1], x0=1, method='brentq')
print ("L1 position in % Earth-CM  units:   ", 100*(1 - sol.root))
print ("L1 position in % Earth-Sun units:   ", 100*(1 - sol.root)/(1 + a))
print ("L1 position similar as in Wikipedia:", 100*(1 - sol.root + a))
