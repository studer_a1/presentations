'''
$ mpirun -n 2 python sgdTestMPI.py
'''
import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler

import warnings
warnings.filterwarnings('ignore')
import sys

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

X = np.array([[-1, -1], [-2, -1], [1, 1], [2, 1]])
Y = np.array([1, 1, 2, 2])

# Always scale the input. The most convenient way is to use a pipeline.
scaler = StandardScaler()
X = scaler.fit_transform(X)

if X.shape[0] % 2 != 0:
    sys.exit()

if rank == 0:
    X = X[::2]; Y = Y[::2]
elif rank == 1:
    X = X[1::2]; Y = Y[1::2]
else:
    sys.exit()

totalIterations = 100
eta = 0.01 #Make sure this equals eta in serial code
eta *= 2 #Factor 2: This leads to w += 2*eta*grad J_r -> sum_r  -> 2w = 2*w_old + 2*eta*grad J
clf = SGDClassifier(max_iter=1, alpha=0, learning_rate='constant', eta0=eta, tol=1e-4, shuffle=False, warm_start=True, random_state=0)

for _ in range(totalIterations):
    clf.fit(X, Y) #clf.coef_ = w_r = w_old + 2*eta*grad J_r ; r = rank; J_r : lossfunction for X_r only
    coef_sum = np.zeros_like(clf.coef_) 
    comm.Allreduce(clf.coef_, coef_sum, op=MPI.SUM) # here coef_sum = 2w = 2*w_old + 2*eta*grad J
    clf.coef_ = coef_sum/2 #the new (common) w (w_old in next iteration); if commented, fits are individual
    comm.barrier()


#Both ranks have same (converged) results if arrived here (same as serial)
np.set_printoptions(precision=3)
print (clf.coef_)

