import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler

import warnings
warnings.filterwarnings('ignore')

np.set_printoptions(precision=3)

X = np.array([[-1, -1], [-2, -1], [1, 1], [2, 1]])
Y = np.array([1, 1, 2, 2])

# Always scale the input. The most convenient way is to use a pipeline.
scaler = StandardScaler()
X = scaler.fit_transform(X)

#Uncomment one of bellow lines to have individual fits (compare with MPI)
#X = X[::2]; Y = Y[::2]
#X = X[1::2]; Y = Y[1::2]

totalIterations = 100
eta = 0.01

clf = SGDClassifier(max_iter=totalIterations, alpha=0, learning_rate='constant', eta0=eta, tol=1e-4, shuffle=False, warm_start=False, random_state=0)
clf.fit(X, Y)
print (clf.coef_)

clf = SGDClassifier(max_iter=1, alpha=0, learning_rate='constant', eta0=eta, tol=1e-3, shuffle=False, warm_start=True, random_state=0)
for _ in range(totalIterations):
    clf.fit(X, Y)

print (clf.coef_)

