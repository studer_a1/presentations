import os
import struct
import numpy as np

class MnistDataLoader():
    def loadData(self, path, kind):
        """Load MNIST data from `path`"""
        labels_path = os.path.join(path, '%s-labels-idx1-ubyte' % kind)
        images_path = os.path.join(path, '%s-images-idx3-ubyte' % kind)
        
        with open(labels_path, 'rb') as lbpath:
            magic, n = struct.unpack('>II', lbpath.read(8))
            labels = np.fromfile(lbpath, dtype=np.uint8)
            
        with open(images_path, 'rb') as imgpath:
            magic, num, nrows, ncols = struct.unpack(">IIII", imgpath.read(16))
            images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), nrows*ncols)
        
        images = ((images/255.0) - 0.5)*2 #scale between -1 .. 1
        return images, labels
    
if __name__ == "__main__":
    mnistDL = MnistDataLoader()
    
    X_train, y_train = mnistDL.loadData('./mnist/', kind='train')
    print('Train Data rows: %d, columns: %d' % (X_train.shape[0], X_train.shape[1]))
    
    X_test, y_test = mnistDL.loadData('./mnist/', kind='t10k')
    print('Test data rows: %d, columns: %d' % (X_test.shape[0], X_test.shape[1]))
    
    print ('Labels:', np.unique(y_train) )

    im_dim = int(np.sqrt(X_train.shape[1]))
    print ('Square image dimension:', im_dim)
    
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True,)
    ax = ax.flatten()
    for i in range(10):
        img = X_train[y_train == i][0].reshape(im_dim, im_dim)
        ax[i].imshow(img, cmap='Greys', interpolation='nearest')

    ax[0].set_xticks([]); ax[0].set_yticks([]); plt.tight_layout()
    # plt.savefig('./figures/mnist_representatives.png', dpi=300)
    plt.show()
    
    fig, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True,)
    ax = ax.flatten()
    for i in range(25):
        img = X_train[y_train == 7][i].reshape(28, 28)
        ax[i].imshow(img, cmap='Greys', interpolation='nearest')

    ax[0].set_xticks([]); ax[0].set_yticks([]); plt.tight_layout()
    # plt.savefig('./figures/mnist_7.png', dpi=300)
    plt.show()
