import numpy as np
import tensorflow as tf
from tensorflow import keras


(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
print("x_train shape:", x_train.shape, "y_train shape:", y_train.shape)
#Shuffle
sampleIndices = np.random.permutation(np.arange(x_train.shape[0]))
x_train = x_train[sampleIndices, :, :]
y_train = y_train[sampleIndices]
# Scale images to the [0, 1] range
x_train = x_train.astype("float32") / 255
x_test = x_test.astype("float32") / 255
print("x_train shape:", x_train.shape, "y_train shape:", y_train.shape)
print(x_train.shape[0], "train samples", x_test.shape[0], "test samples")

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(100, activation='sigmoid'),
  tf.keras.layers.Dense(10)
])

model.summary()

model.compile(
    optimizer=tf.keras.optimizers.SGD(0.005),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

model.fit(x_train, y_train, batch_size=60000//100, epochs=200)
score = model.evaluate(x_test, y_test, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])


