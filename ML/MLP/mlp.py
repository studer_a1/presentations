'''
This python code implements a 3 layer neural network from scratch for didactical reasons
It bases heavily on S. Raschkas code:
https://github.com/rasbt/python-machine-learning-book-2nd-edition/blob/master/code/ch12/ch12.ipynb
I was not fully happy with the math therein: The major source of confusion is that the code 
works only for the special combination of a cross entropy loss and a sigmoidal activation function. 
(The derivative of cross entropy composed with a sigmoid basically yields the identity). 
So if you do the math correctly and generically (calculating the gradient) 
you wonder where the derivative of the last layer activation function disappeared. 
This derivative is explicitly implemented here such that the loss and activation
function can be easily exchanged (Raschkas version is implemented as well, but as a hack)
So this code is equivalent to Raschkas, but with explicit definitions of loss and activation.
'''

import numpy as np
from scipy.special import expit
import sys

from mnistDataLoader import MnistDataLoader

class MLP(object):
    """ Feedforward neural network / Multi-layer perceptron classifier.

    Parameters (for Constructor)
    ------------------------
    X, y: Training features (images) and categories (digit as number)
    X_test, y_test: Testing features and categories 
    n_hidden : int (default: 100) 
        Number of units (neurons) in hidden layer
    l2 : float (default: 0.0)
        Lambda value for L2-regularization. No regularization if l2=0.0 (default)
    epochs : int (default: 200)
        Number of passes over the training set for CG 
    eta : float (default: 0.0005)
        Learning rate. (weight of gradient)
    doShuffle : bool (default: True)
        Shuffles training data every epoch if True to prevent circles.
    numberOfMinibatches : int (default: 100)
        Divides training data into k minibatches for efficiency and stability (do not set =1)
    seed : int (default: None)
        Set seed for random number generator.


    """
    def __init__(self, X, y, X_test, y_test, n_hidden=100,
                 l2=0.01, epochs=200, eta=0.0005, doShuffle=True,
                 numberOfMinibatches=100, seed=1234):

        self.X = X #Training (no explicit index/label)
        self.y = y #    "
        self.X_test = X_test
        self.y_test = y_test
        self.n_output = np.unique(y).shape[0]
        self.n_features = self.X.shape[1]
        self.n_hidden = n_hidden
        self.l2 = l2
        self.epochs = epochs
        self.eta = eta
        self.doShuffle = doShuffle
        self.numberOfMinibatches = numberOfMinibatches
        self.random = np.random.RandomState(seed)
        self.W1, self.W2 = self._initialize_weights()
        self.Y_enc = self._encode_labels(self.y, self.n_output)
        self.doApplyCrossEntropyLoss = False #If True _loss(_derivative) must be MSE, True=HACK
        self.lossFunctionValuesDuringTrainig = []

        
    def printParams(self):
        print ('n_output:', self.n_output)
        print ('n_features:', self.n_features)
        print ('n_hidden:', self.n_hidden)
        print ('l2:' ,self.l2) 
        print ('epochs:', self.epochs)
        print ('eta:', self.eta)
        print ('doShuffle:', self.doShuffle)
        print ('numberOfMinibatches:', self.numberOfMinibatches)
        print ('W1 shape:', self.W1.shape)
        print ('W2 shape:', self.W2.shape)
        
        
    def _encode_labels(self, y, k):
        """Encode labels into one-hot representation

        Parameters
        ------------
        y : array, shape = [n_samples]
            Target values.
        k : number of categories
        
        Returns
        -------
        onehot : array, shape = (n_samples, n_labels)

        """
        onehot = np.zeros((y.shape[0], k))
        for idx, val in enumerate(y):
            onehot[idx, val] = 1.0
        return onehot


    def _initialize_weights(self):
        """Initialize weights with small random numbers.
        
        Returns
        -------
        W1 : array, shape = (n_features + 1, n_hidden)
        W2 : array, shape = (n_hidden + 1, n_output)
        """
        
        W1 = self.random.normal(0.0, 0.1, size=(self.n_features + 1, self.n_hidden))
        W2 = self.random.normal(0.0, 0.1, size=(self.n_hidden + 1, self.n_output))
        W1[0,:] = 0
        W2[0,:] = 0
        return W1, W2


    def _sigmoid(self, z):
        """Compute logistic function (sigmoid)
        Uses scipy.special.expit to avoid overflow error for very small input values z.
        """
        # return 1.0 / (1.0 + np.exp(-z))
        return expit(z)

    def _sigmoid_derivative(self, z):
        return self._sigmoid(z) * (1.0 - self._sigmoid(z))

    def _crossEntropy(self, a, y):
        a[a==0] = 1e-6; a[a==1] = 1-1e-6
        return -y*np.log(a) - (1-y)*np.log(1-a)
    
    def _crossEntropy_derivative(self, a, y):
        a[a==0] = 1e-6; a[a==1] = 1-1e-6
        return -y/a + (1-y)/(1-a)
    
    def _mse(self, a, y):
        return 0.5*(a - y)**2
    
    def _mse_derivative(self, a, y):
        return a - y 
    
    def _activation(self, z):
        return self._sigmoid(z)
    
    def _activation_derivative(self, z):
        return self._sigmoid_derivative(z)
    
    def _loss(self, a, y):
        return self._crossEntropy(a, y)
        return self._mse(a, y)
    
    def _loss_derivative(self, a, y):
        return self._crossEntropy_derivative(a, y)
        return self._mse_derivative(a, y)

    
    def _add_bias_unit(self, X):
        """Add bias unit (as a column)
        Parameters
        ----------
        X : array, shape = [n_1, n_2]
        
        Returns
        ----------
        X : array, shape = [n_1, n_2 + 1]
        """
        X_new = np.ones((X.shape[0], X.shape[1] + 1))
        X_new[:, 1:] = X
        return X_new
    

    def _forwardPropagate(self, X):
        """  
        Parameters
        ----------
        X : array, shape = [n_samples, n_features]
        Input layer with original features.

        Parameters from Member Variables
        --------------------------------        
        W1 : array, shape = [n_features+1, n_hidden_units]
            Weight matrix for input layer -> hidden layer.
        W2 : array, shape = [n_hidden_units+1, n_output_units]
            Weight matrix for hidden layer -> output layer.

        Returns
        ----------
        A1 : array, shape = [n_samples, n_features+1]
            Input values with bias unit.
        Z2 : array, shape = [n_samples, n_hidden]
            Net input of hidden layer.
        A2 : array, shape = [n_samples, n_hidden+1]
            Activation of hidden layer.
        Z3 : array, shape = [n_samples, n_output_units]
            Net input of output layer.
        A3 : array, shape = [n_samples, n_output_units]
            Activation of output layer.

        """
        A1 = self._add_bias_unit(X)#[n_samples, n_features+1]
        Z2 = np.dot(A1, self.W1) #[n_samples, n_features+1] x [n_features+1, n_hidden_units]
        A2 = self._activation(Z2) #   [n_samples, n_hidden_units]
        A2 = self._add_bias_unit(A2) #   [n_samples, n_hidden_units+1]
        Z3 = np.dot(A2, self.W2) # [n_samples, n_hidden_units+1] x [n_hidden_units+1, n_output_units]
        A3 = self._activation(Z3) #[n_samples, n_output_units]
        return A1, Z2, A2, Z3, A3   
    
    
    def _backwardPropagate(self, Y_enc, A1, Z2, A2, Z3, A3):
        """
        Parameters
        ------------
        Y_enc : array, shape = [n_samples, n_labels]
            one-hot encoded class labels.
        A1 : array, shape = [n_samples, n_features+1]
            Input values with bias unit.
        Z2 : array, shape = [n_samples, n_hidden]
            Net input of hidden layer.
        A2 : array, shape = [n_samples, n_hidden+1]
            Activation of hidden layer.
        Z3 : array, shape = [n_samples, n_output_units]
            Net input of output layer.
        A3 : array, shape = [n_samples, n_output_units]
            Activation of output layer.
            
        Parameters from Member Variables
        ----------------------------------  
        W2 : array, shape = [n_hidden_units+1, n_output_units]
            Weight matrix for hidden layer -> output layer.

        Returns
        ---------
        grad_loss_W1 : array, shape = [n_hidden_units, n_features]
            Gradient of the weight matrix w1.
        grad_loss_W2 : array, shape = [n_hidden_units+1, n_output_units]
            Gradient of the weight matrix w2.
        """
        delta_2 = self._loss_derivative(A3, Y_enc) #[n_samples, n_output_units]
        if not self.doApplyCrossEntropyLoss: #Terrible hack, both math and design: remember that for 
            #loss = crossEntropy, there is: Derivative[Loss o sigmoid] = A3 - Y = Derivative[MSE]
            #The reason for this hack is potential numerical instability for direct use of crossEntropy loss(_derivative)
            delta_2 *= self._activation_derivative(Z3) #<- For MSE loss (and all other generic losses)
        grad_loss_W2 = np.dot(A2.T, delta_2) # [n_hidden+1, n_samples] x [n_samples, n_output_units]
        delta_1 = np.dot(delta_2, self.W2.T)[:, 1:] * self._activation_derivative(Z2) #([n_samples, n_output_units] x [n_output_units, n_hidden_units+1])[:,1:]
        grad_loss_W1 = np.dot(A1.T, delta_1) #[n_features+1, n_samples] x [n_samples, n_hidden_units]
    
        return grad_loss_W1, grad_loss_W2


    def learn(self):       
        """ Learn (fit) according training data
        
        Parameters from Member Variables
        --------------------------------
        X : array, shape = [n_samples, n_features]
            Input layer with original features.
        Y_enc : array, shape = [n_samples, n_labels]
            one-hot encoded class labels.
        W1 : array, shape = [n_features+1, n_hidden_units]
            Weight matrix for input layer -> hidden layer.
        W2 : array, shape = [n_hidden_units+1, n_output_units]
            Weight matrix for hidden layer -> output layer.
        numberOfMinibatches
        """

        for ep in range(self.epochs):
            sys.stderr.write('\rEpoch: %d/%d' % (ep, self.epochs)); sys.stderr.flush()
            sampleIndices = np.arange(self.X.shape[0])
            if self.doShuffle:
                sampleIndices = np.random.permutation(sampleIndices)
            miniBatchIndices = np.array_split(sampleIndices, self.numberOfMinibatches)
            for mb in miniBatchIndices:
                #Neural net based gradient
                A1, Z2, A2, Z3, A3 = self._forwardPropagate(self.X[mb,:])
                grad_loss_W1, grad_loss_W2 = self._backwardPropagate(self.Y_enc[mb,:], A1, Z2, A2, Z3, A3)
                grad_reg_W1, grad_reg_W2 = np.copy(self.W1), np.copy(self.W2)
                grad_reg_W1[0, :] = 0 #dont regularize bias terms (which is first row) 
                grad_reg_W2[0, :] = 0 #     "
                grad_total_W1 = grad_loss_W1 + self.l2*grad_reg_W1
                grad_total_W2 = grad_loss_W2 + self.l2*grad_reg_W2
                self.W1 -= self.eta*grad_total_W1
                self.W2 -= self.eta*grad_total_W2
                
            self.lossFunctionValuesDuringTrainig.append( self._computeLoss(A3, self.Y_enc[mb,:]) ) 


    def predict(self):
        """Predict class labels of test data

        Parameters from Member Variables
        --------------------------------
        X_test : array, shape = [n_samples, n_features]
            Input layer with with to be categorized features.

        Returns:
        ----------
        y_pred : array, shape = [n_samples]
            Predicted class labels.
        """
        A1, Z2, A2, Z3, A3 = self._forwardPropagate(self.X_test)
        self.y_predicted = np.argmax(Z3, axis=1) #Z3 shape = [n_samples, n_output_units]

        
    def _computeLoss(self, A3, Y_enc):        
        """
        Parameters
        ----------
        A3: array of activation, shape = [n_samples, n_output_units]
        Y_enc: array of one hot encoded categories;   "
        Returns: loss function value for current activaton (float)
        -------- 
        """
        return self._loss(A3, Y_enc).sum()
    
        
    def evaluatePrediction(self):
        """
        Returns: fraction of correct predictions (float)
        -------- 
        """
        return np.sum(self.y_test == self.y_predicted)/y_test.shape[0]

    
    def showSomeWrongPredictions(self):
        import matplotlib.pyplot as plt
        wronglyCategorizedImages = self.X_test[self.y_test != self.y_predicted][:5*5]
        wronglyCategorizedLabels = self.y_predicted[self.y_test != self.y_predicted][:5*5]
        correctCategorizedLabels = self.y_test[self.y_test != self.y_predicted][:5*5]
        fig, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True,)
        ax = ax.flatten()
        for i in range(5*5):
            ax[i].imshow( wronglyCategorizedImages[i].reshape((28,28)) )
            t = correctCategorizedLabels[i]
            p = wronglyCategorizedLabels[i]
            ax[i].set_title( 't: %d p: %d' % (t, p) )
        ax[0].set_xticks([])
        ax[0].set_yticks([])
        plt.tight_layout()
        plt.show()
        plt.plot(range(self.epochs), self.lossFunctionValuesDuringTrainig)
        plt.title('Loss function'); plt.xlabel('epochs'); plt.ylabel('loss')
        plt.show()
        



if __name__ == "__main__":
    
    print ('Loading MNIST Data')
    print ('------------------')
    mnistDL = MnistDataLoader()
    
    X_train, y_train = mnistDL.loadData('./mnist/', kind='train')
    print('Train Data rows: %d, columns: %d' % (X_train.shape[0], X_train.shape[1]))
    
    X_test, y_test = mnistDL.loadData('./mnist/', kind='t10k')
    print('Test data rows: %d, columns: %d' % (X_test.shape[0], X_test.shape[1]))
    
    print ('Labels:', np.unique(y_train) )

    im_dim = int(np.sqrt(X_train.shape[1]))
    print ('Square image dimension:', im_dim)
    print ()
    print ('Initialize MLP class')
    print ('--------------------')
    
    mlp = MLP(X_train, y_train, X_test, y_test)
    mlp.printParams()
    print ()
    print ('Start learning...')
    mlp.learn()
    print ()
    print ('Predicting image labels...')
    mlp.predict()
    print ()
    print ('Evaluation...')
    print ('Percentage of correct predictions:', 100*mlp.evaluatePrediction() )
    mlp.showSomeWrongPredictions()

        
