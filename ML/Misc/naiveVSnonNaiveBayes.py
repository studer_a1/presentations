from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB

import numpy as np
    
class GaussianNNB: #Non Naive Gaussian Bayes Learner ('supervised soft clustering')
    def __init__(self):
        self._model = {}    
    
    #Implement same API as for scikit-learn class (Role Model)
    def fit(self, X_train, y_train):
        classes = np.unique(y_train)
        for c in classes:
            samplesPerClass = X_train[y_train == c]
            meanOfClass = np.mean(samplesPerClass, axis=0)
            sigmaOfClass = np.cov(samplesPerClass.T)
            priorOfClass = y_train[y_train == c].shape[0]/y_train.shape[0]
            self._model[c] = [meanOfClass, sigmaOfClass, priorOfClass]
        return self
    
    def _predict(self, x): #single feature
        from scipy.stats import multivariate_normal
        argmaxFunction = {}
        for c, c_value in self._model.items():
            meanOfClass, sigmaOfClass, priorOfClass = c_value
            probability_x_given_c = multivariate_normal.pdf(x, mean=meanOfClass, cov=sigmaOfClass)
            argmaxFunction[c] = priorOfClass*probability_x_given_c
        return max(argmaxFunction, key=argmaxFunction.get)
    
    #Implement same API as for scikit-learn class
    def predict(self, X): #array of features
        return np.array([self._predict(x) for x in X])
    
    
if __name__ == "__main__":
    X, y = load_iris(return_X_y=True)
    X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.5, random_state=0)

    gnb = GaussianNB()
    y_pred = gnb.fit(X_train, y_train).predict(X_test)

    gnnb = GaussianNNB()
    y_pred_nn = gnnb.fit(X_train, y_train).predict(X_test)
    
    print("Naive mismatch out of %d : %d" % (X_test.shape[0], (y_test != y_pred).sum() ))
    print("Non-naive mismatch out of %d : %d" % (X_test.shape[0], (y_test != y_pred_nn).sum() ))
    #np.testing.assert_array_equal(y_pred, y_pred_nn)
    #Non-naive classifies samples 48 and 55 correctly (wrong by naive), but 37 incorrectly (ok by naive)
    #print ( np.where(y_test != y_pred) )
    #print ( np.where(y_pred != y_pred_nn) )
    
