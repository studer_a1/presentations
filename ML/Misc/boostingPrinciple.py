#from scipy.stats.binom import cdf
from scipy.special import comb

import matplotlib.pyplot as plt

#def weakLearnerEnsembleSuccessProba(weakLearnerSuccessProba, numberOfWeakLearners):
#    return 1 - cdf(1-weakLearnerSuccessProba, numberOfWeakLearners//2)
def weakLearnerEnsembleSuccessProba(weakLearnerSuccessProba, numberOfWeakLearners):
    s = 0 #summed proba for majority vote
    N = numberOfWeakLearners
    p = weakLearnerSuccessProba
    for k in range(N//2 +1, N +1):
        s += comb(N, k)*p**k*(1-p)**(N-k)
    return s
    
weakLearnerSuccessProba = 0.55

numberOfWeakLearnersRange = range(1, 500, 2)
majorityVoteSuccessProbas = []
for numberOfWeakLearners in numberOfWeakLearnersRange:
  #print ("Number of weak learners", numberOfWeakLearners)
  majorityVoteSuccessProbas.append(weakLearnerEnsembleSuccessProba(weakLearnerSuccessProba, numberOfWeakLearners))
 
plt.plot(numberOfWeakLearnersRange, majorityVoteSuccessProbas)
plt.xlabel('Number of Weak Learners (IID)')
plt.ylabel('Success Probability of Ensemble')
plt.title('Boosting: Single Learner Success Proba= %f' %weakLearnerSuccessProba)
plt.show()

