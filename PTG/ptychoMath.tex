\NeedsTeXFormat{LaTeX2e}
\documentclass[11pt,a4paper]{article}
\usepackage{a4,latexsym}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{verbatim}

\author{alain.studer@psi.ch}
\title{Ptychography: Forward and Backward Operator}


\begin{document}
\maketitle

\begin{abstract}
A mathematical approach to ptychography with a focus on a
analytical inversion formula.
\end{abstract}


\section{Forward Operator}
Let $\psi$ denote the scanned sample (the signal), $\psi \in L^2(\mathbb{R}, \mathbb{C})$.
Let $g \in L^2(\mathbb{R}, \mathbb{C})$ be the incoming beam (typically a Gaussian).
What is measured downstream at the detector (in the Fourier regime) is the spectrogram
of the signal, i.e. the magnitude squared of the 
generalized\footnote{Generalized in the sense that for the Gabor
transform the real space window is a Gaussian} Gabor transform $V_g$, where the Gabor transform 
(a special/optimal STFT) is defined as

\begin{equation}
V_g[\psi](k, \xi) := 
\mathcal{F} \big[ \psi \cdot \tau_{\xi}[g] \big] (k)
=
\mathcal{F}_{1} \big[ \psi(x) g(x - \xi) \big] (k, \xi)
\end{equation}

$\tau_{\xi}, \xi \in \mathbb{R}$ denotes a shift operator, and $\mathcal{F}_{1}$ denotes the
one-dimensional Fourier transform with respect to the first arguments. Hence the measured dataset
$|V_g[\psi]|^2$ is in $L^2(\mathbb{R}^2, \mathbb{R})$. \newline
To summarize, the ptychography operator $\mathcal{P}_g$ reads

\begin{equation}
\mathcal{P}_g: L^2(\mathbb{R}, \mathbb{C}) \rightarrow L^2(\mathbb{R}^2, \mathbb{R})
\quad \quad
\psi(x) \rightarrow \mathcal{P}_g[\psi](k, \xi) := |V_g[\psi]|^2 (k, \xi)
\end{equation}

\section{Backward Operator}
$\mathcal{P}$ can be inverted analytically. Let $\mathcal{A}$ denote the operator
\begin{equation}
\mathcal{A}[g](k, \xi) :=
\mathcal{F}_{1} \big[\bar g \cdot \tau_{\xi}[g] \big] (k)
\end{equation}
hence $\mathcal{A}[g] = V_{g}[\bar g]$. With this definition it holds (up to a global phase)

\begin{equation}
\boxed{
\psi(x) = 
\mathcal{F}_{2}^{-1} \Big[ 
\mathcal{F}_2 \big [\mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big] \big]  
/  S\big[\mathcal{A}[g] \big]
\Big] (x , x)
}
\end{equation}
The operator $S$ swaps arguments: $S[f](x, k) := f(k, x)$.

\newpage
\textit{Proof}: 
First we note that the inverse FT of the PT with respect to the first argument yields
the (auto) cross-correlation of the Gabor transform. (Note that the auto-correlation 
is only for first argument, according the FT)

\begin{equation}
\mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big]
=
\mathcal{F}_1^{-1}  \big[|V_g[\psi]|^2 \big]
=
\mathcal{F}_1^{-1} \Big[ 
\overline{\mathcal{F}_1 \big[ \psi \tau_{\xi}[g] \big]} 
\cdot \mathcal{F}_1 \big[ \psi \tau_{\xi}[g]\big]
\Big]
=
\psi \tau_{\xi}[g] \star_1 \psi \tau_{\xi}[g]
\end{equation}
or, including arguments\footnote{Unfortunately we must keep track of arguments since they are to be swapped}
\begin{equation}
\mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big](x,\xi) 
=
\int_{\mathbb{R}}  \bar \psi(x')\bar g(x' - \xi) \psi(x + x')g(x + x' - \xi)\, dx'
\end{equation}
rearranging 
\begin{equation}
\mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big](x,\xi) 
=
\int_{\mathbb{R}} \psi(x + x') \bar \psi(x') g(x + x' - \xi) \bar g(x' - \xi) \, dx'
\end{equation}

According inversion formula (4), next step is to calculate the FT with respect
to second argument ($\xi$)
\begin{equation}
\mathcal{F}_2 \Big[ \mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big] \Big] (x, k)
=
\int_{\mathbb{R}} \psi(x + x') \bar \psi(x')
\bigg\{
\mathcal{F}_2 \big[ g(x + x' - \xi) \bar g(x' - \xi) \big](x,k; x')
\bigg\}
\, dx'
\end{equation}
According the shift property of the FT there is (shifting variable is $x'$),
hence we set $\xi \rightarrow \xi + x'$ such that $-\xi \rightarrow -\xi - x'$ 
\begin{equation}
\mathcal{F}_2 \big[ g(\xi -x - x') \bar g(\xi - x') \big](x,k; x')
=
e^{-ikx'}\mathcal{F}_2 \big[ g(x - \xi) \bar g(-\xi) \big](x,k)
\end{equation}
Hence for this step we are left with
\begin{equation}
\mathcal{F}_2 \Big[ \mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big] \Big] (x, k)
=
\int_{\mathbb{R}} \psi(x + x') \bar \psi(x')
e^{-ikx'}
\, dx' \, \cdot
\mathcal{F}_2 \big[ g(x - \xi) \bar g(-\xi) \big](x,k)
\end{equation}
Writing the ambiguity transform explicitly
\begin{equation}
\mathcal{A}[g](k, x) = \int_{\mathbb{R}}
\bar g(x')g(x'-x)e^{-ikx'} dx'
\end{equation}
and comparing with
\begin{equation}
\mathcal{F}_2 \big[ g(x -\xi) \bar g(-\xi) \big](x,k)
=
\int_{\mathbb{R}} \bar g(-\xi)g(x - \xi)e^{-i k \xi} d\xi
\end{equation}
we see that the right hand sides are equal if $g$ has even parity and hence
\begin{equation}
S \big[\mathcal{A}[g] \big](x, k)
:=
\mathcal{A}[g](k, x)
=
\mathcal{F}_2 \big[ g(\xi -x) \bar g(\xi) \big](x,k)
\end{equation}
such that in the inversion formula the next algorithmic step
(the division by $S \big[\mathcal{A}[g] \big]$) leaves us with the swapped/mirrored
ambiguity transform of $\psi$.
\begin{equation}
\mathcal{F}_2 \big [\mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big] \big]  
/  S\big[\mathcal{A}[g] \big]
 (x , k)
=
\int_{\mathbb{R}} \psi(x' + x) \bar \psi(x')
e^{-ikx'}
\, dx'
\end{equation}

But the right hand side is nothing else but the FT with respect to the second variable
(i.e not the shift variable), such that the final step in the inversion formula
($\mathcal{F}_2$) yields $\psi(x + \xi) \bar \psi(\xi)$:
\begin{equation}
\mathcal{F}_{2}^{-1} \Big[ 
\mathcal{F}_2 \big\{ \mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big] \big\}
/  S\big[\mathcal{A}[g] \big]
\Big] (x , \xi)
= 
\psi(x + \xi) \bar \psi(\xi)
\end{equation}

Setting $x = \xi$ (and noting that left hand side is semi-hermitian\footnote{
$sh:= \mathcal{F}_{2}^{-1} [h/e]$, $h$ herm, $e$ even $\implies 
sh(-x,-\xi) = \bar{sh} (x, -\xi) \implies sh(x,\xi) = \bar{sh} (-x, \xi)$})
yields
\begin{equation}
\mathcal{F}_{2}^{-1} \Big[ 
\mathcal{F}_2 \big\{\mathcal{F}_1^{-1}  \big[\mathcal{P}_g[\psi] \big] \big\}
/  S\big[\mathcal{A}[g] \big]
\Big] (\xi , \xi)
= 
\bar \psi(0) \psi(\xi)
\end{equation}


The proof as well as the reconstruction formula is inspired by \cite{grohs2017stable},
though I was not fully comfortable with the formula (see p. 6 therein), especially the fact
that forward and backward FT's (along same axes) are not balanced is very confusing. Besides, I didn't 
fully understand the proof (very mathematical) this is why it is repeated here in a 'physics style'.
A more elegant proof might be found using Wigner's transform (and its inverse, the Weyl transform)
\footnote{$\int W[\psi \cdot \tau_{\xi}[g] ](x, k; \xi) dx =  \mathcal{P}_g[\psi](k, \xi)$}
\newline For an overview of ptychography, see \cite{manuel}

\section{Connection to cSAX Beamline}
The ptychography problem was formulated as a one-dimensional procedure whereas 
a ptychography scan takes place in two dimension. However, everything derived above
can be generalized straight-forward in $n$ dimensions. \newline
Needless to say that the analytical inversion is not feasible for 'real'
(noisy and sparsely sampled) experimental data.

\section{Example code}
The above proven analytical inversion formula is implemented numerically (in form
of a python script). In turns out that the signal must have compact support, otherwise
it is not properly recovered at the borders. Insofar the spaces involved are
not $L^2$ but better some Schwartz spaces. The inversion procedure is anyhow numerically
delicate and unstable, but works for arbitrary 'compactified' Fourier series signal.
(see code). Actually, signal should be holomorphic but nevertheless have compact support,
this is a contradiction which might be accommodated by multiplying the signal with a Gaussian.
But multiplying the signal with a polynomial function works as well.

\bibliographystyle{plain}
\bibliography{ptychoMath.bib}


\section{Appendix: Gabor-Spectrogram of Fourier series}
Empirical simulation tests suggest that only the ptychography transform 
of Fourier series can be inverted analytically\footnote{In the sense of 'non-iterative'}
with reasonable accuracy. So let's
assume the signal $\psi \in L^2(\mathbb{R}, \mathbb{C})$ is a (finite) Fourier series, 
\begin{equation}
\psi(x)= \sum_{n \in \mathbb{Z}} c_n e^{inx}
\end{equation}
and hence $\psi$ is $2\pi$ periodic. The Gabor transform is linear, such that
\begin{equation}
V_g[\psi](k, \xi) = 
\sum_{n \in \mathbb{Z}} c_n V_g[e^{inx}](k, \xi)
\end{equation}
For simplicity, we assume that $g$ is a real-valued Gaussian pdf
\begin{equation}
V_g[e^{inx}](k, \xi) =
\mathcal{F}_{1} \big[e^{inx}  g(x - \xi; \sigma_B) \big] (k, \xi)
\end{equation}
where $\sigma_B$ denotes the width of the incoming beam. The amplitude-modulated 'plane
wave' is more or less a Eigenfunction of the Fourier transform
\begin{equation}
\mathcal{F}_{1} \big[g(x; \xi, \sigma_B) e^{inx} \big] (k, \xi) = 
g(k; n, \sigma_B^{-1}) e^{-i \xi(k-n)}
\end{equation}
such that the Gabor transform of a Fourier series results in (up to norm)
\begin{equation}
V_g[\psi](k, \xi)
=
\sum_{n \in \mathbb{Z}} c_n 
g(k; n, \sigma_B^{-1}) e^{-i \xi(k-n)}
=
e^{-i \xi k}\sum_{n \in \mathbb{Z}} c_n 
g(k; n, \sigma_B^{-1}) e^{i \xi n}
\end{equation}
This is a superposition of Gaussians centered at $n \in \mathbb{Z}$ times a 
(standing) plane wave (with a phase shift). If $k$ is considered fixed, the
sum can be interpreted as another Fourier series in $\xi$, the mapping\footnote{
Coordinate-vector mapping is clearly linear and (less obvious) also diagonal} being
$c_n \rightarrow g(k; n, \sigma_B^{-1}) c_n$ \newline
If the width of the incoming beam is $\geq 2 \pi$, i.e covering the $2\pi$ interval of 
the periodic signal, the width of the reciprocal space Gaussian $\sigma_k$
equals $1/2\pi$ meaning that $\pm 3 \sigma_k \simeq \pm 1/2$ and consequently $>0.99$
of the 'weight' lies between $-1/2$ and $1/2$. This means that the functions in the above
sum have $\sim$ disjoint support, such that the magnitude squared can be approximated as
\begin{equation}
|V_g[\psi]|^2(k, \xi)
\simeq
\sum_{n \in \mathbb{Z}} |c_n 
g(k; n, \sigma_B^{-1}) e^{-i \xi(k-n)}|^2
=
\sum_{n \in \mathbb{Z}} |c_n |^2
g^2(k; n, \sigma_B^{-1})
\end{equation}
The last term of the above equation describes what the simulation code plots for the
spectrogram (ptychography transform). Though the situation there is a bit more complicated,
since the signal itself is multiplied by a Gaussian $g(x; \sigma_S)$. But since the 
product of two Gaussians is (up to a norm) a Gaussian\footnote{So obviously also square, 
such that in (22): $g^2(k; n, \sigma_B^{-1}) = g(k; \, \mu = n, \sigma^2 = \sigma_B^{-2}/2)$}, 
the above calculation can easily be repeated:
In eq. (20), we substitute  $g(x; \xi, \sigma_B)$ with  $g(x; \xi, \sigma_B)g(x; \sigma_S)$;
generically, it holds 
\begin{equation}
g(x; \, \mu_1, \sigma_1^2)g(x; \, \mu_2, \sigma_2^2) \propto
g(x; \, \mu=\frac{\sum_i \mu_i \sigma_i^2}{\sum_i\sigma_i^2}, \, 
\sigma^2 = \frac{\Pi_{i} \sigma_i^2}{\sum_i\sigma_i^2})
\end{equation}
such that up to generic scale factor
$1/\sqrt{2\pi\sum_i \sigma_i^2} \,\, e^{-\frac{(\mu_1-\mu_2)^2}{2\sum_i \sigma_i^2} }$,
we have
\begin{equation}
g(x; \xi, \sigma_B^2)g(x; \sigma_S^2) \propto
g(x; \mu = \xi \frac{ \sigma_B^2}{\sigma_B^2 + \sigma_S^2}, \,
\sigma^2 = \frac{\sigma_B^2 \sigma_S^2}{\sigma_B^2 + \sigma_S^2})
\end{equation}
In (22) right hand side, only the width of the Gaussians changes, they remain centered
at $\mu = n \in \mathbb{Z}$, also for the two multiplied Gaussians. In code,
$\sigma_B \simeq 2 \sigma_S$ such that $\sim \sigma_B \rightarrow 0.5 \sigma_B$
and hence the width of the spectrogram lines changes as 
$\sigma_B^{-1} \rightarrow 2\sigma_B^{-1}$ when compactifying the FS. \newline
The first order correction to (22) is to include the overlap of adjacent functions, i.e.
the approximation reads \footnote{$|V_g^0[\psi]|^2$ equals label (22) rhs, the superscript 
$0$ highlights the zero order approximation. The zero order approximation always has this form,
also if the condition $\sigma_B > 2\pi$ is not met, but only if this condition is fulfilled,
zero order is a good approximation for the overall ptychography transform.\newline
Furthermore, the $cc$ is derived as follows: the other sum of terms which are 1 integer
apart read $ e^{i\xi}\sum_{n \in \mathbb{Z}} c_n c_{n-1}^{*}
g(k; n, \sigma_B^{-1}) g(k; n-1, \sigma_B^{-1})$. Shifting the summation index by one yields
$ e^{i\xi}\sum_{n \in \mathbb{Z}} c_{n+1} c_{n}^{*} g(k; n+1, \sigma_B^{-1}) g(k; n, \sigma_B^{-1})$.
The phase derives as $e^{-i\xi(k-n)} e^{i\xi(k-(n-1))} = e^{i\xi}$}
\begin{equation}
|V_g^1[\psi]|^2(k, \xi)
=
|V_g^0[\psi]|^2(k)
+
e^{-i\xi} \sum_{n \in \mathbb{Z}} c_n c_{n+1}^{*}
g(k; n, \sigma_B^{-1}) g(k; n+1, \sigma_B^{-1}) + cc
\end{equation}
which is has the form of a (tensor) product and is $2\pi$ periodic in $\xi$ axis:
\begin{equation}
|V_g^1[\psi]|^2(k, \xi)
=
|V_g^0[\psi]|^2(k)
+
G_{\Sigma}(k)\cos(\xi)
\end{equation}
with the sum of weighted/multiplied/shifted Gaussians defined as
\begin{equation}
G_{\Sigma}(k) = \sum_{n \in \mathbb{Z}} 2 \cdot Re \big(c_n c_{n+1}^{*}\big)
g(k; n, \sigma_B^{-1}) g(k; n+1, \sigma_B^{-1})
\end{equation}
The full expansion of the ptychography transform of a Fourier series 
(signal $\psi$ as defined in 17) reads accordingly
\begin{equation}
\boxed{
|V_g[\psi]|^2(k, \xi)
=
\sum_{\Delta n = 0}^{\infty}
G_{\Sigma}(k; \Delta n; \, \psi) \cos(\Delta n \xi)
}
\end{equation}
with\footnote{Here it is important to include (relevant part of the)  
scale factor of the resulting Gaussian that equals the product of the two original Gaussians.
(To see how fast the series converges, namely exponentially with $\Delta n ^2$, plus:
the larger $\sigma_B$ the better the $0$ order approximation).\newline
So to be precise: The product of two Gaussian pdf's is a Gaussian function but usually
not a Gaussian pdf. (If and only if scale factor (on top of this page) equals 1).}
\begin{equation}
G_{\Sigma}(k; \Delta n; \psi) = 
e^{-\frac{1}{4} \sigma_B^2 (\Delta n)^2 }
\cdot
2^{sgn(\Delta n)} 
\sum_{n \in \mathbb{Z}} Re \big(c_n c_{n+\Delta n}^{*}\big)
\cdot
g \big(k; n + \Delta n/2, \sigma_B^{-1}/\sqrt 2 \big)
\end{equation}
Hence besides the Gabor transform, also the ptychography transform of a Fourier series is
a Fourier series itself (in $\xi$; with $k$ dependent coefficients and the $\Delta n$
dependency is such that $\lim_{\Delta n\to\infty} G_{\Sigma}(k; \Delta n) = 0 \, \forall k$, fast).\newline
Finally note that the formulas in this section are purely mathematical in the sense that 
$x, k, \xi$ and all $\sigma$'s are all unitless numbers. (Such that $n$ can be a unitless integer).


\end{document}

