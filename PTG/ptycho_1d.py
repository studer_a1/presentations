'''
This python code is supposed to demonstrate the
analytical inversion of the ptychography transform
and also to highlight how unstable the procedure is.
Only compactified Fourier series can be reconstructed.
In principle this is no restriction, since a
Fourier series can approximate an arbitrary signal.
Also a compact support is in principle no restriction
since this models the (smooth) border of a detector;
however, de facto the reconstruction capability is limited,
even for some Fourier series (the ~discontinuous ones).
But the main feature of ptychography, namely to recover
a complex-valued signal from (a special set of) 
its magnitudes, is proven possible. 
('Ptychography: A solution to the phase problem')
Theory (which is the documentation of this code) 
can be found in this repo folder (ptychoMath.tex)
According theory (eq 15) setting xi = 0 should work 
as well but is numerically less optimal (than (xi, xi)).
A particularly interesting and amazing property of the 
inverse operator is its near linearity (only the zero 
point normalisation of the reconstructed signal breaks it).
Finally note that the term 'inverse ptychography transform'
is a bit too optimistic, the beam should be a Gaussian for
reasonable reconstruction quality, so the term 'inverse
magnitude squared Gabor transform' would be more appropriate.
Author: A. Studer, SciComp, PSI
'''

import numpy as np
import matplotlib.pyplot as plt


def gauss1D(n, mu, sig):
    def gaussian(x, mu, sig):
        return np.exp(-0.5*((x - mu)/sig)**2)
    x = np.arange(-n//2, n//2, 1.0)
    return gaussian(x, mu, sig)#*(np.exp(0.5*1j*(x - mu)/(2*np.pi))).real #Gaussian only..
    
    
def calculateGaussWindows(n, sigma, phase, offset):
    g = np.zeros((n, n)).astype('complex')
    for i in range(n): #number of raster positions = number of detector bins and ..
        shiftedWindow = gauss1D(n, i - n/2.0 + offset, sigma)#..raster step-size maps to bin-size
        g[:,i] = shiftedWindow #shift is second coordinate
    #plt.imshow(g.real); plt.show() 
    #g = np.flipud(g) # **) change from row ordering to coord-system ordering...
    return g*np.exp(1j*phase) #...skipped, since (x,xi) does not form a coord system
   

def calculateGeneralizedGaborTransform(f, g):
    gGT = np.fft.fft((g.T*f).T, axis=0) #fft first coordinate
    return gGT


def calculatePtychographyTransform(f, g):
    return np.abs(calculateGeneralizedGaborTransform(f, g))**2


def calculateInversePtychographyTransform(p, g, f_0):
    iFP1 = np.fft.ifft(p, axis=0) #FFT first coord
    FP2iFP1 = np.fft.fft(iFP1, axis=1)#FFT second coord
    n = p.shape[0] #~middle coord (~: depending on even or odd number of pixels)
    AgS = np.fft.fft((np.conj(g[:, n//2])*g.T), axis=1) #swapped ambiguity transform
    AgS[AgS.real == 0.0] = np.finfo(float).eps #Numerical, delicate for real part of rec
    FP2iFP1 /= AgS #Divide by swapped ambiguity transform
    iFP2FP2iFP1 = np.fft.ifft(FP2iFP1, axis=1) #inverse FFT second coordinate
    iFP2FP2iFP1 = np.fft.fftshift(iFP2FP2iFP1) #? why needed, should be consistent without?
    f_rec = np.copy(np.diag(iFP2FP2iFP1)) #(anti-)diagonal in 2d array is result, depending on **)
    #f_rec = iFP2FP2iFP1[:, n//2] #~works, but zero point index is not well defined + shift-axis=0?
    f_rec_0 = f_rec[n//2]; f_rec *= f_0/np.conj(f_rec_0) #recover original f(0) value
    return f_rec

    
def defineSignal(numberOfPixels): #Example signal is (compactified) Fourier series, must be analytic
    x = (2*np.pi)*np.arange(-numberOfPixels//2, numberOfPixels//2, 1.0)/numberOfPixels
    f = np.zeros(numberOfPixels).astype('complex')
    numberOfFrequencies = 4  #Chose 4 random amplitudes and frequencies
    amplitudes_r = np.random.uniform(0, 1, numberOfFrequencies)     #real and..
    amplitudes_i = np.random.uniform(0, 1, numberOfFrequencies)*1.j #..imag part of FS coefficients
    amplitudes = amplitudes_r + amplitudes_i #should all have the same sign (?)
    #amplitudes = [1 for i in range (numberOfFrequencies)] #fixed amplitudes for reproducibility
    nyquistFrequency = numberOfPixels//2 #Freq spectrum between [-nyquistFrequency, nyquistFrequency]
    maxFreq = nyquistFrequency//2 #chosen smaller than Nyquist frequency, for visibility
    frequencies = np.random.randint(-maxFreq, maxFreq, size=numberOfFrequencies)
    #frequencies = 1*np.array([ 1, 1, -1, -1]) #fixed frequencies for reproducibility
    #frequencies = amplitudes = [1 for i in range (numberOfFrequencies)] #check base freq.
    print ("Signal Frequencies:", frequencies); print ("Signal Amplitudes:", amplitudes)
    for nu, a in zip(frequencies, amplitudes):
        f += a*np.exp(nu*1j*x)
    #Confine signal, else can't be recovered at border (Fourier series has no compact support)
    sampleWindow_gauss = gauss1D(n=numberOfPixels, mu=0.0, sig=numberOfPixels/10)
    f *= sampleWindow_gauss #(only in theory analytic, in experiment cropped compact support)
    return f


def defineSignalNoFourierSeries(numberOfPixels): #Reconstruction fails
    x = np.pi*np.arange(numberOfPixels)/(numberOfPixels/2)
    nu = 5; a = 1; n = numberOfPixels
    f = a*np.exp(nu*1j*x**2); #position dependent frequency (freq increases linearly)
    #f[:n//2] = a*np.exp(10j*x[:n//2]); f[n//2:] = a*np.exp(20j*x[n//2:]) #2 separated freqs
    #f[:n//2] = 0 #..or non-analytic signal breaks reconstruction
    sampleWindow_gauss = gauss1D(n=numberOfPixels, mu=0.0, sig=numberOfPixels/10)
    f *= sampleWindow_gauss
    return f
    
    
def addPoissonNoise(signal, scale):
    std = scale*np.sqrt(np.abs(signal)) #Poisson noise itself is too large
    noisySignal = signal + np.random.normal(scale=std) + 1j*np.random.normal(scale=std)
    return noisySignal


def defineProbe(numberOfPixels):
    sigma = numberOfPixels/np.pi #Don't chose too small (division by zero)
    offset = 0.7 #numerical, delicate; correct for 'no middle' index in even sized array
    phase = np.pi/3 #whatever (numerically tolerant)
    g = calculateGaussWindows(numberOfPixels, sigma=sigma, phase=phase, offset=offset)
    return g


def plotResults(g, f, f_rec, p):
    plt.plot(g[:,g.shape[0]//2].real, label='Beam (Probe) real', color='b')
    plt.plot(g[:,g.shape[0]//2].imag, label='Beam (Probe) imag', color='g'); plt.legend(); plt.show()
    plt.plot(f.real, label='Signal real', color='b'); plt.legend()
    plt.plot(f.imag, label='Signal imag', color='g'); plt.legend(); plt.show()    
    plt.imshow(np.log(p)); plt.xlabel('Position [arb. units]');  plt.ylabel('Frequency [arb. units]')
    plt.title('Log of PS of STFT (Spectrogram)'); plt.show() #standard is frequency on y axis
    #plt.plot(p[0,:], label='P(xi)', color='g'); plt.legend(); plt.show() #c_{-1}=1, c_1=1, rest=0
    c = numberOfPixels//10 #Crop plot at border due to evtl instabilities
    plt.plot(f.real[c:-c], label='Signal ori real'); plt.legend()
    plt.plot(f_rec.real[c:-c], label='Signal rec real'); plt.legend(); plt.show()
    plt.plot(f.imag[c:-c], label='Signal ori imag'); plt.legend()
    plt.plot(f_rec.imag[c:-c], label='Signal rec imag'); plt.legend(); plt.show()
    
    
if __name__ == "__main__":
    numberOfPixels = 2**10 #Number of detector bins (even numerically better than odd?)
    #Define the probe (rastered beam) as a complex Gaussian, width is delicate
    g = defineProbe(numberOfPixels)
    #Define signal as a compactified Fourier series with (4) random frequencies and amplitudes
    f = defineSignal(numberOfPixels)
    #Add noise (if scale > 0) to see break down of reconstruction (very noise sensitive)
    f = addPoissonNoise(f, scale = 0.0)
    #Calculate Spectrogram (|STFT|^2), here called Ptychography transform
    p = calculatePtychographyTransform(f, g)    
    #Reconstruct input signal from spectrogram (zero value of ori signal is needed)
    f_rec = calculateInversePtychographyTransform(p, g, f[numberOfPixels//2])
    #Plot results:
    plotResults(g, f, f_rec, p)
