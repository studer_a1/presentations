'''
This python script should demonstrate
that the logarithmic spiral has
(multiple, discrete) scale invariances.
Also visible in animation:
Scale + Rotation = continuous symmetry.
Since theta only covers a finite range,
the symmetry is 'broken' (especially
visbible at the end of the animation cycle)
'''
#module load psi-python27/4.4.0

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation 
#Scale parameters to cover in animation
beta = np.linspace(0.0, 2*np.pi, 100)
#angular array
theta = np.linspace(-6*np.pi, 6*np.pi, 800)
#Plot settings
fig = plt.figure(figsize=(6,6))
# Next two lines (values) from https://de.wikipedia.org/wiki/Datei:Logarithmic_Spiral_Pylab.svg
ax = plt.axes([0.1, 0.1, 0.8, 0.8], polar = True)
kappa = np.log(1.19)
#Reference r(theta)
r = np.exp(kappa*theta)
#r = theta #linear
#Plot reference Spiral
ax.plot(theta, r)
line, = ax.plot([],[])

#Plot scaled spirals
def update(b):
    r_s = np.exp(b)*r #scales reference spiral
    #r_s = 3*b*r #linear
    line.set_xdata(theta) #update data in plot (theta unchanged)
    line.set_ydata(r_s)   #      "
    return line,

#Define and run animation
a = FuncAnimation(fig, update, frames=beta, blit=True)
plt.show()

