'''
This python code implements
the FFT algorithm and compares
the result with numpy FFT.
2016, A.S., Scientific Computing, PSI. 
'''
import numpy as np

#FFT implementation according Gauss' idea (Cooley Tukey)
def FFT(x):
    if x.shape[0] == 1:
        return x
    x_odd  = x[1::2]
    x_even = x[::2]
    p = np.exp( -2*np.pi*1j*np.arange(x.shape[0]/2)/float(x.shape[0]) ) 
    p = np.concatenate((p,-p))
    return p*np.tile(FFT(x_odd), 2) + np.tile(FFT(x_even), 2)

#Unit test (check against numpy fft)
if __name__ == '__main__':
    t = np.arange(0.0, 2*np.pi, np.pi/2**10) #time domain
    print ("input array size:", t.shape[0], "(should be power of 2)")
    omega_1 = 100; omega_2 = 500; omega_3 = 1000 #frequencies
    s = np.sin(omega_1*t) + np.cos(omega_2*t) + np.sin(omega_3*t) #signal
    fs = FFT(s)
    fs_ref = np.fft.fft(s)
    print ("L2 difference real part", ((fs.real - fs_ref.real)**2).sum())
    print ("L2 difference imag part", ((fs.imag - fs_ref.imag)**2).sum())
