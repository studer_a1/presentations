import numpy as np
from scipy import optimize

import matplotlib.pyplot as plt

def find_a_x_l(y_sea, x_s):
    def fun(x):
        x_ = (x_s - x[1])/x[0]
        return [x[0]*(np.cosh(x_) - 1) - y_sea, x[1] + x[0]*np.sinh(x_) - y_sea]
    return optimize.anderson(fun, [1, -1])


def kettenLinie(y_sea, x_s):
    a, x_l = find_a_x_l(y_sea, x_s)
    #print (x_s, x_l, a)
    x = np.arange(x_l, x_s, 0.01)
    y = a*(np.cosh((x - x_l)/a) -1)
    return x, y


def potentielleEnergie(y_sea, x_s):
    a, x_l = find_a_x_l(y_sea, x_s)
    x = (x_s + np.abs(x_l))/a
    return a**2 * ( 0.25*np.sinh(2*x) + x - np.sinh(x) )


def potentielleEnergieApprox(y_sea, x_s):
    return 2*x_s**3/y_sea + (2/3)*y_sea*x_s
    


y_sea = 10
x_s_min = 1
x_s_max = 10
for x_s in np.arange(x_s_min, x_s_max, 1):
   x, y = kettenLinie(y_sea, x_s)
   plt.plot(x, y, label="x_s=" + str(x_s))
   plt.xlabel("Meeresgrund [meter]"); plt.ylabel("Tiefe [meter]")
   #plt.plot(x_s, y_sea, 's', color='black'); plt.legend()
   plt.plot(x_s, y_sea, '^', color='black'); plt.legend()
plt.show()

E_p = []; E_p_a = []
dx = 0.3
x_s_values = np.arange(x_s_min, x_s_max-4, dx)
for x_s in x_s_values:
    E_p.append(potentielleEnergie(y_sea, x_s))
    E_p_a.append(potentielleEnergieApprox(y_sea, x_s))
plt.plot(x_s_values, E_p, label="Potentielle Energie der Kette");
plt.plot(x_s_values, E_p_a, label="1. Ord. Potentielle Energie der Kette");
plt.xlabel("Bootsauslenkung [meter]"); plt.legend(); plt.ylabel("Arb. Units"); plt.show()
plt.plot(x_s_values, np.gradient(E_p, edge_order=2), label="Kraft auf Boot");
plt.plot(x_s_values, np.gradient(E_p_a, edge_order=2), label="1. Ord. Kraft auf Boot");
plt.xlabel("Bootsauslenkung [meter]"); plt.legend(); plt.ylabel("Arb. Units"); plt.show()

