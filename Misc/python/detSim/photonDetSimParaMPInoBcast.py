#Calculate integrated image based on a list of detector coordinates 
#representing measurements of single photon events. MPI based implementation.
#To run:
#$ module load psi-python38/2020.11
#$ mpirun -n 20 python photonDetSimParaMPInoBcast.py

import numpy as np
import time

from functools import partial

from mpi4py import MPI

def simulateDetData(numberOfHits, detSize_x, detSize_y):
    #Simulate (random) data. (List/array of detector coordinates)
    coord = np.zeros((numberOfHits, 2), dtype=np.int)
    coord[:, 0] = np.random.randint(low=0, high=detSize_y, size=numberOfHits)
    coord[:, 1] = np.random.randint(low=0, high=detSize_x, size=numberOfHits)
    return coord

def detHistnp(coord, detSize_x, detSize_y):
    h = partial(np.histogram2d, bins=[np.arange(detSize_y+1), np.arange(detSize_x+1)])
    partialHistogram = h(coord[:,0], coord[:,1])
    return partialHistogram[0]  #[0]: Check histogram2d docu


if __name__ == "__main__":
    #MPI def/init
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    numberOfCores = comm.Get_size()

    #Definition of detector and number of photons (hitting detector)
    numberOfHits = int(1e8) #number of photons
    #numberOfHits = 122345  #To check error handling
    #Detector Image
    detSize_x = 1400
    detSize_y = detSize_x + 1 #test for non-square detector

    numberOfHitsPerCore = numberOfHits // numberOfCores
    if numberOfHits % numberOfCores != 0:
        print ("Missing some data! (", numberOfHits - numberOfCores*numberOfHitsPerCore, "photon(s) )")

    #Simulate data on rank 0 and send coord stripes (hits) to the rest
    if rank == 0:
        simDetData = simulateDetData(numberOfHits, detSize_x, detSize_y)
        st = time.time()
        hitsPerCore2receive = simDetData[0:numberOfHitsPerCore, :] #Not really received, but consistent with other ranks
        for r in range(1, numberOfCores):
            hitsPerCore2send = simDetData[r*numberOfHitsPerCore:(r+1)*numberOfHitsPerCore, :]
            hitsPerCore2send = np.ascontiguousarray(hitsPerCore2send, dtype=np.int)
            comm.Send([hitsPerCore2send, MPI.LONG], dest=r)
    #Receive what was sent by 0 on all other ranks
    else:
        hitsPerCore2receive = np.empty(2*numberOfHitsPerCore, dtype=np.int)
        comm.Recv([hitsPerCore2receive, MPI.LONG], source=0)
        hitsPerCore2receive = hitsPerCore2receive.reshape((numberOfHitsPerCore, 2))

    #Each rank has its data (0 inclusive), now sum up partial histograms (on 0)
    partialHist = detHistnp(hitsPerCore2receive, detSize_x, detSize_y)
    partialHist = np.ascontiguousarray(partialHist, dtype=np.int)
    integratedHist = np.ascontiguousarray(np.zeros((detSize_y, detSize_x), dtype=np.int))
    comm.Reduce(partialHist, integratedHist, op=MPI.SUM, root=0)

    #Check consistency 
    if rank == 0:
        print ("MPI histogram time (", numberOfCores, "cores ):", time.time() - st, "sec" )
        print ("Number of photons", integratedHist.sum(), "should equal", numberOfHits)

