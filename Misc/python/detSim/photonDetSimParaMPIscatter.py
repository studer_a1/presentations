#Calculate integrated image based on a list of detector coordinates 
#representing measurements of single photon events. MPI based implementation.
#To run:
#$ module load psi-python38/2020.11
#$ mpirun -n 20 python photonDetSimParaMPIscatter.py

import numpy as np
import time

from functools import partial

from mpi4py import MPI

def simulateDetData(numberOfHits, detSize_x, detSize_y):
    #Simulate (random) data. (List/array of detector coordinates)
    coord = np.zeros((numberOfHits, 2), dtype=np.int)
    coord[:, 0] = np.random.randint(low=0, high=detSize_y, size=numberOfHits)
    coord[:, 1] = np.random.randint(low=0, high=detSize_x, size=numberOfHits)
    #print (coord.shape, coord.dtype)
    return coord


def detHistnp(coord, detSize_x, detSize_y):
    h = partial(np.histogram2d, bins=[np.arange(detSize_y+1), np.arange(detSize_x+1)])
    partialHistogram = h(coord[:,0], coord[:,1])
    return partialHistogram[0]  #[0]: Check histogram2d docu


if __name__ == "__main__":
    #MPI def/init
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    numberOfCores = comm.Get_size()

    #Definition of detector and number of photons (hitting detector)
    numberOfHits = int(1e8) #number of photons
    #numberOfHits = 122345  #To check error handling
    #Detector Image
    detSize_x = 1400
    detSize_y = detSize_x + 1 #test for non-square detector
  
    numberOfHitsPerCore = numberOfHits // numberOfCores
    if numberOfHits % numberOfCores != 0:
        print ("Missing some data! (", numberOfHits - numberOfCores*numberOfHitsPerCore, "photon(s) )")
 
    #Simulate data on rank 0 and send (scatter) coord stripes (hits) to the rest
    simDetData = None 
    if rank == 0:
        simDetData = simulateDetData(numberOfHits, detSize_x, detSize_y)
        st = time.time()
    #Receive what was sent by 0 on all other ranks
    hitsPerCore2receive = np.empty(2*numberOfHitsPerCore, dtype=np.int)
    comm.Scatter(simDetData, hitsPerCore2receive, root=0)
    hitsPerCore2receive = hitsPerCore2receive.reshape((numberOfHitsPerCore, 2))    
    #Each rank has its raw data (0 inclusive), now sum up partial histograms (on 0).
    #For a 'real' application (no simulation), rank specific data would typically
    #be loaded from a (huge) hdf5 file (hitsPerCore2receive = hdf5.read...)
    partialHist = detHistnp(hitsPerCore2receive, detSize_x, detSize_y)
    partialHist = np.ascontiguousarray(partialHist, dtype=np.int)
    integratedHist = np.ascontiguousarray(np.zeros((detSize_y, detSize_x), dtype=np.int))
    comm.Reduce(partialHist, integratedHist, op=MPI.SUM, root=0)

    #Check consistency 
    if rank == 0:
        print ("MPI histogram time (", numberOfCores, "cores ):", time.time() - st, "sec" )
        print ("Number of photons", integratedHist.sum(), "should equal", numberOfHits)
    
    #Plot
    #if rank == 0:
    #    import matplotlib.pyplot as plt
    #    plt.imshow(imageArray2); plt.show()


