#Calculate integrated image based on a list of detector coordinates 
#representing measurements of single photons. Calculation done in two different ways.
#Python multithread Ref: https://iscinumpy.gitlab.io/post/histogram-speeds-in-python/

import numpy as np
import time

from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from functools import partial

def simulateDetData(numberOfHits, detSize_x, detSize_y):
    #Simulate (random) data. (List/array of detector coordinates)
    coord = np.zeros((numberOfHits, 2)).astype('int')
    coord[:, 0] = np.random.randint(low=0, high=detSize_y, size=numberOfHits)
    coord[:, 1] = np.random.randint(low=0, high=detSize_x, size=numberOfHits)
    return coord

def detHistNaive(coord, detSize_x, detSize_y):
    #Loop fill in versus..
    imageArray = np.zeros((detSize_y, detSize_x)).astype('int')
    for i in range (coord.shape[0]):
        imageArray[coord[i,0],coord[i,1]] += 1
    return imageArray

def detHistnp(coord, detSize_x, detSize_y, numberOfCores):
    #..Histogram function: np serial is ~5 times faster, with parallel 20 cores
    #this gives and ideal speedup of ~100; de facto speedup ~50
    chunkSize = coord.shape[0] // numberOfCores
    if coord.shape[0] % numberOfCores != 0:
        print ("Missing some data! (", coord.shape[0] - numberOfCores*chunkSize, "photon(s) )")
    h = partial(np.histogram2d, bins=[np.arange(detSize_y+1), np.arange(detSize_x+1)])
    #Split input data in equal chunks, calculate histogram for every chunk
    with ThreadPoolExecutor(max_workers=numberOfCores) as pool: #Thread faster than Process
    #with ProcessPoolExecutor(max_workers=numberOfCores) as pool:
        coord_chunks_0 = [coord[i*chunkSize:(i+1)*chunkSize, 0] for i in range(numberOfCores)]
        coord_chunks_1 = [coord[i*chunkSize:(i+1)*chunkSize, 1] for i in range(numberOfCores)]
        partialHistograms = pool.map(h, coord_chunks_0, coord_chunks_1)
        #no need to wait explicitly, this is done implicitly by context manager, see e.g
        #https://stackoverflow.com/questions/21143162/python-wait-on-all-of-concurrent-futures-threadpoolexecutors-futures
    return partialHistograms

def sumupPartialHistograms(partialHistograms, detSize_x, detSize_y):
    #Sum up partial results to get overall histogram
    imageArray2 = np.zeros((detSize_y, detSize_x)).astype('int')
    for ph in partialHistograms:
        imageArray_part, xedges, yedges = ph
        imageArray2 += imageArray_part.astype('int')
    #Convert explicitly to int (for unit test)
    imageArray2 = imageArray2.astype('int')
    return imageArray2


if __name__ == "__main__":
    #Definition of detector and number of photons (hitting detector)
    numberOfHits = int(1e8) #number of photons
    #Detector Image
    detSize_x = 1400
    detSize_y = detSize_x + 1 #test for non-square detector
    numberOfCores = 20 #Run on e.g Ra

    simDetData = simulateDetData(numberOfHits, detSize_x, detSize_y)

    st = time.time()
    imageArray = detHistNaive(simDetData, detSize_x, detSize_y)
    print ("Loop histogram time:", time.time() - st, "sec" )

    st = time.time()
    partialHist = detHistnp(simDetData, detSize_x, detSize_y, numberOfCores)
    imageArray2 = sumupPartialHistograms(partialHist, detSize_x, detSize_y)
    print ("Parallel Numpy histogram time (", numberOfCores, "cores ):", time.time() - st, "sec" )

    #Ceck consistency
    print ("Number of photons",imageArray2.sum(), "should equal", numberOfHits)
    #Unit test
    isEqual = np.array_equal(imageArray, imageArray2)
    print ("Image arrays equal:", isEqual)
    if not isEqual:
        print ("Difference in integrated number of photons:", imageArray.sum() - imageArray2.sum() )

    #Plot
    #import matplotlib.pyplot as plt
    #plt.imshow(imageArray2); plt.show()

