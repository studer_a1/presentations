#Calculate integrated image based on a list of detector coordinates 
#representing measurements of single photon events. MPI based implementation.
#To run:
#$ module load psi-python38/2020.11
#$ mpirun -n 20 python photonDetSimParaMPI.py

import numpy as np
import time

from functools import partial

from mpi4py import MPI


def simulateDetData(numberOfHits, detSize_x, detSize_y):
    #Simulate (random) data. (List/array of detector coordinates)
    coord = np.zeros((numberOfHits, 2)).astype('int')
    coord[:, 0] = np.random.randint(low=0, high=detSize_y, size=numberOfHits)
    coord[:, 1] = np.random.randint(low=0, high=detSize_x, size=numberOfHits)
    #print (coord.shape, coord.dtype)
    return coord


def detHistnp(coord, detSize_x, detSize_y, rank, numberOfCores):
    #..Histogram function: np serial is ~5 times faster, with parallel 20 cores 
    #this gives and ideal speedup of ~100; de facto speedup ~50
    chunkSize = coord.shape[0] // numberOfCores
    if coord.shape[0] % numberOfCores != 0:
        print ("Missing some data! (", coord.shape[0] - numberOfCores*chunkSize, "photon(s) )")
    h = partial(np.histogram2d, bins=[np.arange(detSize_y+1), np.arange(detSize_x+1)])
    #Split input data in equal chunks, calculate histogram for every chunk
    coord_chunks_0 = coord[rank*chunkSize:(rank+1)*chunkSize, 0]
    coord_chunks_1 = coord[rank*chunkSize:(rank+1)*chunkSize, 1]
    partialHistogram = h(coord_chunks_0, coord_chunks_1)
    return partialHistogram[0]  #[0]: Check histogram2d docu


if __name__ == "__main__":
    #MPI def/init
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    numberOfRanks = comm.Get_size()

    #Definition of detector and number of photons (hitting detector)
    numberOfHits = int(1e8) #number of photons
    #Detector Image
    detSize_x = 1400
    detSize_y = detSize_x + 1 #test for non-square detector

    #Simulate and broadcast data; inefficient to send all data (but irrelevant for toy case)
    if rank == 0:
        simDetData = simulateDetData(numberOfHits, detSize_x, detSize_y)
        simDetData = np.ascontiguousarray(simDetData, dtype=np.int)
    else:
        simDetData = np.ascontiguousarray(np.empty((numberOfHits, 2), dtype=np.int))
    #print (rank, simDetData.dtype, simDetData.shape)
    comm.Bcast(simDetData, root=0)
    #All ranks have all data (more than needed, better: send only the parts extracted in detHistnp)
    if rank == 0:    st = time.time()
    partialHist = detHistnp(simDetData, detSize_x, detSize_y, rank, numberOfRanks)
    partialHist = np.ascontiguousarray(partialHist, dtype=np.int)
    integratedHist = np.ascontiguousarray(np.zeros((detSize_y, detSize_x), dtype=np.int))
    comm.Reduce(partialHist, integratedHist, op=MPI.SUM, root=0)

    #Check consistency
    if rank == 0:
        print ("Parallel Numpy histogram time (", numberOfRanks, "cores ):", time.time() - st, "sec" )
        print ("Number of photons", integratedHist.sum(), "should equal", numberOfHits)
    #Plot
    #if rank == 0:
    #    import matplotlib.pyplot as plt
    #    plt.imshow(imageArray2); plt.show()

