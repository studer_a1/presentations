import numpy as np
from scipy.ndimage import laplace

import pylab as plt

def mu(c, gamma):
    return c**3 - c - gamma*laplace(c)

def update(c, dt, gamma):
    c_new = c + dt*laplace(mu(c, gamma))
    return c_new

n = 100
gamma = 0.5
dt = 0.01
signexp = np.random.randint(2, size=(n,n))
c = (-np.ones((n,n)))**signexp
print (c.sum())
#plt.imshow(c, cmap = 'hot', interpolation='none',vmin=-1,vmax=1); plt.show()
im = None
for i in range(1000):
    if not im:
        # for the first frame generate the plot...
        im = plt.imshow(c, cmap = 'gray', vmin=-1, vmax=1)
        plt.pause(0.2)
    else:
        # ... for subsequent times only update the data
        c = update(c, dt, gamma)
        im.set_data(c)
        if i % 100 == 0:
            print (c.sum())
    if i % 2 == 0:
        plt.draw()
        plt.pause(0.02)
    
