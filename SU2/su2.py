'''
This script is to demonstrate the equivalence between SO(3) and SU(2) based rotations.
Since SU(2) is isomorphic to SO(3) (modulo sign), every U in SU(2) has a 3 dimensional
representation D(U) in SO(3). Let s=(s_i) denote the Pauli matrices and let S denote the basis
change S: R^3 -> H, v -> v.s,  H being the space of traceless Hermitian 2x2 matrices, then
the 3D representation D(U) reads D(U): R^3 -> R^3; D(U)v := S^-1 U* S(v) U.
Proof (sketch): D(U) is a linear operator, since all involved mappings are linear. 
Since ||v|| = det(S(v)), ||D(U)v|| = |S S^-1 U* S(v) U| = |U* S(v) U| = |S(v)| = ||v|| it is
D(U) in O(3). For a fixed rotation axis n and rotation angle a, the one dimensional[*] 
Lie (sub-)group can be parameterized as U(n,a) = cos(a/2) + i n.s sin(a/2). From 'a/2' one can see 
that rotation angle could be in [0, 4pi) and that for angles in [2pi, 4pi) the matrix U changes sign.
So for one R(n,a) in S0(3) we have two U's in SU(2) (+- U), hence SU(2)/(+-1) ~ SO(3).
Topology: O(3) is not connected (+/-1 branches), SO(3) is connected but not simply connected,
SU(2) is simply connected (double covers SO(3)). Helps to proof that D is bijection.
The difference between Pauli matrices and Quaternions is the 'i' convention and sequence labels.
Disclaimer: For a group homomorphism, it should be D(U_2 U_1) = D(U_2)D(U_1), so
formally S^-1 U S(v) U* and U(n,a) = cos(a/2) - i n.s sin(a/2) is the correct choice. <- Implemented.
Proof that U(n,a) -> R(n,a) are the corresponding rotations: either direct calculation 
with pencil and paper (tedious) or, like here, computational proof.
Also interesting: matrix representation formula: D(U)_ij = 1/2 Tr(s_i U s_j U*); i,j=1..3.
[*] The rotation axis n can be parameterized by its polar angles so in total we get back
the full 3 dimensional Lie group (parameterized by 3 specific Euler angles).
'''

import numpy as np


def _S(v):
    """
    Parameters: vector v in R^3 as numpy array shape (3) 
    Returns: inner product of v with Pauli matrices as np array shape (2,2)
    """
    s_1 = np.asarray([[0,1], [1,0]])    #Pauli matrices as components and ....
    s_2 = np.asarray([[0,-1j], [1j,0]]) #        "
    s_3 = np.asarray([[1,0], [0,-1]])   #        "
    s = np.asarray([s_1, s_2, s_3])     #....aggregated as vector (np array of shape (3,2,2) )
    return (v[:, np.newaxis, np.newaxis]*s).sum(axis=0)


def _S_inv(H):
    """
    Parameters: Traceless Hermitian Matrix H as (2,2) numpy array
    Returns: corresponding vector v in R^3 as numpy array shape (3)
    """    
    return np.asarray([H[0,1].real, -H[0,1].imag, H[0,0].real])

                      
def rotate_SU2(v, n, a, U = None): #QM version (analoguous to Quaternions)
    """
    Parameters: vector v in R^3 as numpy array shape (3)
                rotational axis n in R^3 as numpy array shape (3)
                rotational angle a in [0, 2pi) by which we rotate around n
                U is optional SU(2) rotation matrix, taking precedence over (n,a) defined rotation
    Returns: rotated vector in R^3 as numpy array shape (3) + operator (as numpy array shape (2,2))
    """
    if U is None: #Build U in SU2 (based in rotation defined by (n,a))
        U = np.cos(a/2)*np.eye(2) - 1j*np.sin(a/2)*_S(n) #'Coords' of U: (cos a/2, -i*sin a/2 n) in C^4
    return _S_inv(np.dot(U, np.dot(_S(v), U.conj().T))), U


def rotate_SO3(v, n, a, R = None):
    """
    Parameters: same/analoguous as for SU(2) version
    Returns: same/analoguous as for SU(2) version
    """
    if R is None: #Build R in SO3 (based in rotation defined by (n,a))
        R = np.cos(a)*np.eye(3) + np.sin(a)*np.cross(np.eye(3), n) + (1 - np.cos(a))*np.outer(n, n)
    return np.dot(R, v), R


def unitTest():
    """
    a) Checks if the two different implementations of a rotation around fixed axis match
    b) Check that 'quaternion' rotation obeys a group structure, i.e. a product of SU(2) 
       rotation matrices corresponds to composition of respective rotations
    c) Check SU(2) property
    """
    print ("Unit Tests")
    print ("**********")
    #a) Rotation can be implemented with Pauli matrices
    n = np.random.randn(3); n /= np.linalg.norm(n)
    a = 2*np.pi*np.random.rand()
    v = np.random.randn(3)
    v_rot_SU2, U_1 = rotate_SU2(v, n, a)
    v_rot_SO3, R_1 = rotate_SO3(v, n, a)
    print ("SU(2) vs SO(3) rotation: all close:", np.allclose(v_rot_SU2, v_rot_SO3) )
    #b1) Second rotation (composition with first)
    n = np.random.randn(3); n /= np.linalg.norm(n)
    a = 2*np.pi*np.random.rand()
    v_rot_SU2, U_2 = rotate_SU2(v_rot_SU2, n, a)
    v_rot_SO3, R_2 = rotate_SO3(v_rot_SO3, n, a)
    print ("Composition of rotation: all close:", np.allclose(v_rot_SU2, v_rot_SO3) )
    #b2) check group/representation structure
    U_12 = np.dot(U_2, U_1)
    v_rot_SU2_U_12, _ = rotate_SU2(v, n = None, a = None, U = U_12)
    print ("Representation property SU(2): all close:", np.allclose(v_rot_SU2, v_rot_SU2_U_12) )
    R_12 = np.dot(R_2, R_1)
    v_rot_SO3_R_12, _ = rotate_SO3(v, n = None, a = None, R = R_12)
    print ("Representation property SO(3): all close:", np.allclose(v_rot_SO3, v_rot_SO3_R_12) )
    #c) Finally check SU(2)/SO(3) property
    print ("SU(2) defining property: all close:", np.allclose(np.linalg.inv(U_12), U_12.conj().T) )
    print ("SO(3) defining property: all close:", np.allclose(np.linalg.inv(R_12), R_12.T) )
    print ("Finished")
    print ("********")


if __name__ == "__main__":
    unitTest()
    
