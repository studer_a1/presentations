'''
This class generates key pairs
based on elliptic curve cryptography.
As a reference example, the secp256k1 standard
(used by Bitcoin) is implemented. secp256k1
elliptic curve equation: y^2 = x^3 + 7
i.e a=0, b=7 in the generic equation
y^2 = x^3 + ax + b; for p and starting point
G see the 'initAccording_secp256k189' method.
Algorithm implemented according 
https://de.wikipedia.org/wiki/Elliptische_Kurve#Gruppenoperation
Also recommendable:
http://andrea.corbellini.name/2015/05/17/elliptic-curve-cryptography-a-gentle-introduction/
'''
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class ECKG(object):
    def __init__(self, a, b, p, G):
        """
        Defines all values necessary for generating a key pair
        """
        self.a = a #elliptic curve parameter
        self.b = b #    "
        self.p = p #Group order (must be prime)
        self.G = G #Initial element on EC
        self.r = self.p #Largest Value for private key
        self.isSecp256k1 = False
    
    def initAccording_secp256k1(self):
        """
        As constructor, but specifying secp256k1 standard
        """
        self.a = 0
        self.b = 7
        self.p = 2**256 - 2**32 - 2**9 - 2**8 - 2**7 - 2**6 - 2**4 - 1
        G_x = 55066263022277343669578718895168534326250603453777594175500187360389116729240
        G_y = 32670510020758816978083085130507043184471273380659243275938904335757337482424
        self.G = [G_x, G_y]
        self.r = 2**256 #max size of private key (random number r)
        self.isSecp256k1 = True
        
    def _eea(self,i, j):
        """
        Extended euclidian algorithm used to calculate 
        multiplicative inverse in file Z_p
        From: https://www.johannes-bauer.com/compsci/ecc/#anchor12
        
        Args: (int, int)
        
        Returns: ((int, int, int)) 
        
        Describing decomposition (gcd(i, j), m, n)
        where m*i + n*j = gcd(i, j)
        
        """
        (s, t, u, v) = (1, 0, 0, 1)
        while j != 0:
            (q, r) = (i // j, i % j)
            (unew, vnew) = (s, t)
            s = u - (q * s)
            t = v - (q * t)
            (i, j) = (j, r)
            (u, v) = (unew, vnew)
        (d, m, n) = (i, u, v)
        return (d, m, n)


    def _multiplicativeInverse(self, j):
        """
        calculate j^-1 for input j (in Z_p)
    
        Args: (int)
        
        Returns: (int)
        """
        (d, m, n) = self._eea(self.p, j)
        return m % self.p
    
    def _doublePoint(self, p_e):
        """
        Doubling a point p_e on ellitic curve (p_2 -> 2p_e)
        'neutralElement' is with respect to this addition
        
        Args: (Curve point)
        
        Returns: (Curve point)
        """
        
        if p_e == 'neutralElement':
            return 'neutralElement' #0+0=0
        if p_e[1] == 0:
            return 'neutralElement'
        #assert self.p[1] != 0
        s = (3*p_e[0]**2 + self.a) * self._multiplicativeInverse(2*p_e[1])
        x = s**2 - 2*p_e[0]
        y = -p_e[1] + s*(p_e[0] -x)
        return [x % self.p, y % self.p]

    def _addPoints(self, p_e_1, p_e_2):
        """
        Definition of addition for the elliptic curve group
        
        Args: (Curve point, Curve point)
        
        Returns: (Curve point)

        """
        if p_e_1 == 'neutralElement':
            return p_e_2
        if p_e_2 == 'neutralElement':
            return p_e_1
        if (p_e_1[0] == p_e_2[0]) and (p_e_1[1] == p_e_2[1]):
            return self._doublePoint(p_e_1)
        if p_e_1[0] == p_e_2[0]:
            if p_e_1[1] != - p_e_2[1] % self.p:
                print ('Unexpected..')
            return 'neutralElement'

        s = (p_e_1[1] - p_e_2[1]) % self.p
        s *= self._multiplicativeInverse( (p_e_1[0] - p_e_2[0]) % self.p) 
        x = s**2 - p_e_1[0] - p_e_2[0]
        y = -p_e_1[1] + s*(p_e_1[0] -x)
        return [x % self.p,y % self.p]

    def _scalarMultiply(self, n, p_e_0):
        """
        Multiple, repeated additions of curve points.
        This is not a group multiplication, it is analgous
        to scalar multiplication in a vector space.
        
        Args: (int, Curve point)
        
        Returns: Curve point

        """        
        p_e = p_e_0
        #for i in xrange(n): #Numpy cannot handle large numbers
        i = 0
        while i < n:
            p_e = self._addPoints(p_e, p_e_0)
            #print i, p_e, checkIfOnEllipse(p_e)
            i += 1
        return p_e


    def _scalarMultiplyByDoubling(self, n, p_e_0):
        """
        Mathematically equivalent to the above, but runs in
        time O(log(n)) compared to O(n) (n = private key)
        This is crucial since the reverse operation can 
        only be done in O(n) time. (Brute force search)
        Also note that doubling a point does not double runtime
        (Going e.g from 4G to 8G needs one curve opreation, not 4)
        
        Args: (int, Curve point)
        
        Returns: Curve point
        """

        numberOfBinaryDigitsInPrivateKey = int(math.ceil(math.log(n, 2)))
        result = 'neutralElement' #Init to zero (It's not [0,0])
        point2Add = p_e_0
        
        for b in range(numberOfBinaryDigitsInPrivateKey):
            if n & 1:
                result = self._addPoints(result, point2Add)
            n = n >> 1
            point2Add = self._doublePoint(point2Add)
        
        return result
        
        
        
    def _isPointOnEllipticCurve(self, p_e):
        """ 
        Checks if a point from (Z_p x Z_p) is on elliptic curve
        
        Args: (Curve point)
        
        Returns: (Boolean)
        """
        if p_e == 'neutralElement':
            return True
        return (p_e[0]**3 - p_e[1]**2 + self.b) % self.p == 0
    
    
    def _generateLargeRandomNumber(self, maxRandint):
        """
        Numpy can only handle small random numbers,
        so we 'stitch' them here. (Not ideal)
        
        Args: (int)
        
        Returns: (int)

        """
        numpyRandIntMax = 2**16
        r = 1
        e = int(np.floor(math.log(maxRandint, 2)))
        for i in range(e//16):
            r *= np.random.randint(numpyRandIntMax)
        return r
    
    
    #Public methods start here
    def findValidPoint(self):
        """
        Find a valid (initial) point G on the curve for 
        given Z_p and elliptic curve. (Pure helper function)
        
        Returns (Curve point)
        """
        p = self.getPrimeOrder()
        G_test = [0, 0]
        while not self._isPointOnEllipticCurve(G_test):
            G_test = [np.random.randint(p), np.random.randint(p)]
        return G_test
        
    def generateKeyPair(self):
        """
        Calculates a key pair
        """
        if self.isSecp256k1:
            self.privateKey = self._generateLargeRandomNumber(self.r)
            self.publicKey  = self._scalarMultiplyByDoubling(self.privateKey, self.G)
        else:
            self.privateKey = np.random.randint(self.r)
            self.publicKey  = self._scalarMultiply(self.privateKey, self.G)
        
    

    def isPublicKeyValid(self, p_e):
        """
        Same as above, except that neutralElement
        is not considered a valid public key
        
        Args: (Curve point)
        
        Returns: (Boolean)
        """
        if p_e == 'neutralElement':
            return False
        return self._isPointOnEllipticCurve(p_e)
        
    
    def displayKeyPair(self):
        if self.isPublicKeyValid( self.getPublicKey() ):
            print ("Private Key", self.getPrivateKey())
            print ("Public  Key", self.getPublicKey())
        else:
            print ("Key genaration failed")
        
    #Unit test, Visualization and Standard 'API'
    def unitTest_secp256k1(self):
        """
        Test implemented algorithm against a private/public key pair
        copied from the O'Reilly Bitcoin book
        """
        self.initAccording_secp256k1()
        k = int("1E99423A4ED27608A15A2616A2B0E9E52CED330AC530EDCC32C8FFC6A526AEDD", 16)
        K = self._scalarMultiplyByDoubling(k, self.G)
        assert self.isPublicKeyValid(K)
        K_x = K[0]; K_y = K[1]
        K_x_ref = int("F028892BAD7ED57D2FB57BF33081D5CFCF6F9ED3D3D7F159C2E2FFF579DC341A", 16)
        K_y_ref = int("07CF33DA18BD734C600B96A72BBC4749D5141C90EC8AC328AE52DDFE2E505BDB", 16)
        K_ref = [K_x_ref, K_y_ref]
        assert self.isPublicKeyValid(K_ref)
        assert K_x == K_x_ref
        assert K_y == K_y_ref
        
        
    #Getter an setter
    def getStartingPoint(self):
        return self.G
    
    def getPrimeOrder(self):
        return self.p
    
    def getPrivateKey(self):
        return self.privateKey
    
    def getPublicKey(self):
        return self.publicKey
    
    def setMaxPrivateKeySize(self, r):
        self.r = r

    #Visualization only
    def visualizeCurve(self):
        p = self.getPrimeOrder()
        G = self.getStartingPoint()
        ec_graph = np.zeros((p, p))
        for i in range(2*p):
            G_n = self._scalarMultiply(i, G)
            if G_n != 'neutralElement':
                ec_graph[G_n[0], G_n[1]] = 1
            else:
                ec_graph[0, 0] = 1

        plt.matshow(ec_graph)
        plt.show()
    
    #from https://stackoverflow.com/questions/10429556/animate-quadratic-grid-changes-matshow
    def visualizeKeyWalk(self):
        p = self.getPrimeOrder()
        self._n = 0
        def generate_data():
            domain = np.zeros((p,p)) #Reset
            if self._n < self.getPrivateKey():
                self._n += 1
            G_n = self._scalarMultiply(self._n, self.getStartingPoint())
            if G_n == 'neutralElement':
                domain[0, 1] = 1
            else:
                domain[G_n[0], G_n[1]] = 1
            return domain
        
        def update(data):
            mat.set_data(data)
            return mat 

        def dataGenerator():
            while True:
                yield generate_data()

        fig, ax = plt.subplots()
        mat = ax.matshow(generate_data())
        ani = animation.FuncAnimation(fig, update, dataGenerator, interval=1000, save_count=2*p)
        plt.show()



class LSKG(ECKG):
    """
    This class uses the same procedure for key generation as its parent class, 
    except that addition is defined in linear space.
    This is to demonstrate the strength of the key generation on the elliptic curve.
    (The linear pattern is very predictable.)
    Note that the design is not good, since the relationship between LSKG and ECKG
    is not of the form 'is a'. (There are no elliptic curve parameters which could
    turn the curve ECKG into a straight line LSKG). Nevertheless, it works..
    """
    def __init__(self, p, G):
        super(LSKG, self).__init__(0, 0, p, G)
        
    def _addPoints(self, p_e_1, p_e_2):
        """
        Definition of addition for the vector space group
        Overwrites parent class method
        """
        p = self.getPrimeOrder()
        return [(p_e_1[0] + p_e_2[0]) % p, (p_e_1[1] + p_e_2[1]) % p]
    
    
    def isPublicKeyValid(self, p_e):
        """
        Check for valid public key
        Overwrites parent class method
        """        
        return True
        #Better:
        raise NotImplementedError
    
    
    
if __name__ == "__main__":
    #'''
    eckg = ECKG(a=0, b=0, p=0, G=[0,0])
    eckg.unitTest_secp256k1()
    eckg.generateKeyPair()
    eckg.displayKeyPair()
    #'''
    eckg = ECKG(a=0, b=7, p=53, G=[9,43])
    #print eckg.findValidPoint()
    eckg.generateKeyPair()
    eckg.displayKeyPair()
    eckg.visualizeCurve()
    eckg.visualizeKeyWalk()
    #'''
    lskg = LSKG(p=53, G=[2,6])
    lskg.generateKeyPair()
    lskg.displayKeyPair()
    lskg.visualizeCurve()
    lskg.visualizeKeyWalk()

